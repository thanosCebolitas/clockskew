var csMap = [
    {
        name : "barrel",

        // render object
        type : "plane",
        pos : [-128, 32, 128],
        size : [32, 64, 0],
        rot : [0, 0, 0],
        inGameType : "object",

        // collision object
        cType : "cube",
        cSize : [32, 64, 32],
        cRot : [0, 0, 0],

        // external references
        texRef : "barrel",
        renderEffects : null,
        scriptRef : "barrel_script",
        characterSheet : null
    },
    {
        name : "barrel",

        // render object
        type : "plane",
        pos : [128, 32, 128],
        size : [32, 64, 0],
        rot : [0, 0, 0],
        inGameType : "object",

        // collision object
        cType : "cube",
        cSize : [32, 64, 32],
        cRot : [0, 0, 0],

        // external references
        texRef : "barrel",
        renderEffects : null,
        scriptRef : "barrel_script",
        characterSheet : null
    },
    {
        name : "barrel",

        // render object
        type : "plane",
        pos : [-128, 32, -128],
        size : [32, 64, 0],
        rot : [0, 0, 0],
        inGameType : "object",

        // collision object
        cType : "cube",
        cSize : [32, 64, 32],
        cRot : [0, 0, 0],

        // external references
        texRef : "barrel",
        renderEffects : null,
        scriptRef : "barrel_script",
        characterSheet : null
    },
    {
        name : "bucket",

        // render object
        type : "plane",
        pos : [128, 16, -128],
        size : [32, 32, 0],
        rot : [0, 0, 0],
        inGameType : "object",

        // collision object
        cType : "cube",
        cSize : [32, 32, 32],
        cRot : [0, 0, 0],

        // external references
        texRef : "bucket",
        renderEffects : null,
        scriptRef : "barrel_script",
        characterSheet : null
    },
    {
        name : "ground",

        // render object
        type : "plane",
        pos : [0, 0, 0],
        size : [1024, 1024],
        rot : [-Math.PI/2, 0, 0],
        inGameType : "ground",

        // collision object
        cType : "cube",
        cSize : [1024, 5, 1024],
        cRot : [-Math.PI/2, 0, 0],

        // external references
        texRef : "wall1",
        renderEffects : null,
        scriptRef : null,
        characterSheet : null
    },
    {
        name : "StartPosition",

        // render object
        type : "plane",
        pos : [0, 32, 0],
        size : [64, 64],
        rot : [0, 0, 0],
        inGameType : "character",

        // collision object
        cType : "cube",
        cSize : [64, 64, 64],
        cRot : [0, 0, 0],

        // external references
        texRef : 'humanMale',
        renderEffects : null,
        scriptRef : null,
        characterSheet : null
    },
    {
        name : "Monster1",

        // render object
        type : "plane",
        pos : [-256, 42, 0],
        size : [64, 84],
        rot : [0, 0, 300],
        inGameType : "character",

        // collision object
        cType : "cube",
        cSize : [64, 84, 64],
        cRot : [0, 0, 0],

        // external references
        texRef : 'skeleton',
        renderEffects : null,
        scriptRef : null,
        characterSheet : null
    }
];
