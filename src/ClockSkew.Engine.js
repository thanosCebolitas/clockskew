// ------------------------------------
// Global Vars
// ------------------------------------
var frameNum = 0;
var animationTime = 20;
var timeDiff = 0;
var stats;
var mouse = new THREE.Vector2();
var mouseClient = new THREE.Vector2();
var hasMouseMoveEvent = false;
var hasMouseClickEvent = false;
var three;
var shiftDown = false;
var ctrlDown = false;

// merge sort
// ------------------------------------
var sort = function(array) {
    var len = array.length;
    if(len < 2) { 
        return array;
    }
    var pivot = Math.ceil(len/2);
    return merge(sort(array.slice(0,pivot)), sort(array.slice(pivot)));
};

var merge = function(left, right) {
    var result = [];
    var camPos = three.camera.position;
    var index = 0;

    while((left.length > 0) && (right.length > 0)) {
        if(left[0].position.distanceTo(camPos) > right[0].position.distanceTo(camPos)) {
            left[0].renderDepth = index;
            result.push(left.shift());
        }
        else {
            right[0].renderDepth = index;  
            result.push(right.shift());
        }
        index = index - 1;
    }

    result = result.concat(left, right);
    return result;
};

// requestAnimFrame
// ------------------------------------
window.requestAnimFrame = ( function(callback){
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(callback){
        window.setTimeout(callback, 1000 / 60);
    };
} )();


function loadJS(file) {
    // DOM: Create the script element
    var jsElm = document.createElement("script");
    // set the type attribute
    jsElm.type = "application/javascript";
    // make the script element load file
    jsElm.src = file;
    // finally insert the element to the body element in order to load the script
    document.body.appendChild(jsElm);
}

// HID
// ------------------------------------

function snapToGrid( pos ) {
    var x = pos.x - ( pos.x % 64 ) + ( (pos.x > 0) ? 32 : -32 );
    var y = pos.y; 
    var z = pos.z - ( pos.z % 64 ) + ( (pos.z < 0) ? -32 : 32 );
    pos.set(x, y, z);
}

function hoverObject( obj ) {
    // unselect previous
    if ( three.objUnderMouse !== null && three.objUnderMouse && (three.objUnderMouse !== obj) &&
        ( three.objSelected === null || three.objSelected !== obj) ) {
        three.objUnderMouse.material.color.setHex(0xFFFFFF);
    }

    // store new
    three.objUnderMouse = obj;

    // remove previous bounding and collision boxes
    three.scene.remove(three.objBoundingBox);
    three.scene.remove(three.objCollisionBox);
    three.objBoundingBox = null;
    three.objCollisionBox = null;

    // if there is a new selection
    if ( obj && ( three.objSelected === null  || three.objSelected !== obj ) ) {
        var hex = 0xFF00FF;

        // add new
        three.objBoundingBox = new THREE.BoundingBoxHelper( three.objUnderMouse, hex );
        three.objBoundingBox.update();
        three.objCollisionBox = new THREE.BoundingBoxHelper( three.objUnderMouse, hex );
        three.objCollisionBox.update();
        three.scene.add(three.objBoundingBox);
        three.scene.add(three.objCollisionBox);

        three.objUnderMouse.material.color.setHex(0xFF0000);

        // remove grid selector
        three.scene.remove(three.gridSelector);
    }
}

// function to select object in rendered scene
// "selection" includes bounding box, color shift, and collision box
function selectObjectInScene( obj ) {
    // unselect previous
    if ( three.objSelected !== null && three.objSelected && obj && (three.objSelected !== obj) ) {
        three.objSelected.material.color.setHex(0xFFFFFF);
    }

    // store new
    three.objSelected = obj;

    var hex = 0xFFFF00;
    // remove previous bounding and collision boxes
    three.scene.remove(three.objBoundingBoxSel);
    three.scene.remove(three.objCollisionBoxSel);
    if( three.objSelected === three.objUnderMouse ) {
        three.scene.remove(three.objBoundingBox);
        three.scene.remove(three.objCollisionBox);
        three.objBoundingBox = null;
        three.objCollisionBox = null;
    }

    // add new
    if ( obj ) {
        three.objBoundingBoxSel = new THREE.BoundingBoxHelper( three.objSelected, hex );
        three.objBoundingBoxSel.update();
        three.objCollisionBoxSel = new THREE.BoundingBoxHelper( three.objSelected, hex );
        three.objCollisionBoxSel.update();
        three.scene.add(three.objBoundingBoxSel);
        three.scene.add(three.objCollisionBoxSel);

        three.objSelected.material.color.setHex(0x0000FF);
    }
}

function isPixelDrawnByObject( x, y, obj ) {
    var nObj = obj.clone();
    three.ppsScene.add(nObj);
    three.renderer.clear();
    three.renderer.render(three.ppsScene, three.camera);

    var gl = three.renderer.getContext();
    var pixelData = new Uint8Array(4);
    gl.readPixels(x, y, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, pixelData);
    three.ppsScene.remove(nObj);

    if ( pixelData[0] || pixelData[1] || pixelData[2] ) {
        //console.log(true, nObj, x, y, pixelData);
        return true;
    }
    //console.log(false, nObj, x, y, pixelData);
    return false;
}

function onDocumentMouseMove( event ) {
    event.preventDefault();

    if ( csComponents.handleMouseMoveEvent(event) ) {
        three.controls.enabled = false;
        return;
    }
    else {
        three.controls.enabled = true;
    }

    var height = three.CANVAS_HEIGHT;
    var width = three.CANVAS_WIDTH;

    mouse.x = ( ( event.clientX - three.CANVAS_OFFSET_LEFT ) / width ) * 2 - 1;
    mouse.y = - ( ( event.clientY - three.CANVAS_OFFSET_TOP ) / height ) * 2 + 1;
    mouseClient.x = event.clientX;
    mouseClient.y = event.clientY;
    hasMouseMoveEvent = true;
}

function onDocumentMouseDown( event ) {
    event.preventDefault();

    //---------------------
    // check if ui wants the event
    if (csComponents.handleMouseDownEvent(event)) {
        three.controls.enabled = false;
        return;
    }
    else {
        three.controls.enabled = true;
    }

    //---------------------
    // prepare ray
    var height = three.CANVAS_HEIGHT;
    var width = three.CANVAS_WIDTH;

    mouse.x = ( ( event.clientX - three.CANVAS_OFFSET_LEFT ) / width ) * 2 - 1;
    mouse.y = - ( ( event.clientY - three.CANVAS_OFFSET_TOP ) / height ) * 2 + 1;

    // ray cast - if intersects floor and ctrl | cmd is pressed
    shiftDown = event.shiftKey;
    ctrlDown = event.ctrlKey || event.metaKey;
    hasMouseClickEvent = true; 
}

function onDocumentMouseUp( event ) {        
    event.preventDefault();

    if(csComponents.handleMouseUpEvent(event)) {
        three.controls.enabled = false;
        return;
    }
    else {
        three.controls.enabled = true;
    }

}

var keymap = []; // Or you could call it "key"
var handleKeyEvents = function(e){
    e = e || event; // to deal with IE
    keymap[e.keyCode] = e.type == 'keydown';
    /*insert conditional here*/

    three.player.mDir1 = (keymap[87] === true); // up : w
    three.player.mDir2 = (keymap[83] === true); // down : s
    three.player.mDir3 = (keymap[65] === true); // left : a
    three.player.mDir4 = (keymap[68] === true); // right : d

 /* case 38: // up (arrow)
        case 40: // down (arrow)
        case 37: // left(arrow)
        case 39: // right (arrow)
        case 32: // space
        // set camera to default position
        case 69: // (e) */

    shiftDown = e.shiftKey;

    // TODO: remove
    if ( e.keyCode == 81 ) { // (q)
            three.characterSheet.EqBodyParts.FEET = "Metal Boots";
            updateObjectTexture(three.player);
    }
};

onblur = function(){
    keymap = [];
    if(three) {
        three.player.mDir1 = false;
        three.player.mDir2 = false;
        three.player.mDir3 = false;
        three.player.mDir4 = false;
    }
};

function animateHandleMouseMove() {
        hasMouseMoveEvent = false;
        var height = three.CANVAS_HEIGHT;
        var width = three.CANVAS_WIDTH;
        var vector = new THREE.Vector3(mouse.x, mouse.y, 1);
        var raycaster = projector.pickingRay(vector.clone(), three.camera);
        var xToCheck = mouseClient.x - three.CANVAS_OFFSET_LEFT;
        var yToCheck = height - mouseClient.y + three.CANVAS_OFFSET_TOP;

        // test against objects
        var intersects = raycaster.intersectObjects(three.objects, false);
        var consumed = false;
        if ( intersects.length > 0 ) {
            for ( var ind2Obj in intersects ) { 
                var intObj = intersects[ind2Obj].object;
                if ( isPixelDrawnByObject(xToCheck, yToCheck, intObj)  ) {
                    hoverObject(intObj);
                    consumed = true;
                    break;
                }
            }
        }

        if ( !consumed ) {
            hoverObject( null );

            // check against grid
            intersects = raycaster.intersectObjects([three.grid], false);
            if ( intersects.length > 0 ) {
                var p = intersects[0].point;
                snapToGrid(p);
                three.scene.add(three.gridSelector);
                three.gridSelector.position.set(p.x, 32, p.z);
            }
            else {
                three.scene.remove(three.gridSelector);
            }
        }
}

function animateHandleMouseClick() {
    hasMouseClickEvent = false; 
    var height = three.CANVAS_HEIGHT;
    var width = three.CANVAS_WIDTH;
    var vector = new THREE.Vector3( mouse.x, mouse.y, 1 );
    var raycaster = projector.pickingRay( vector.clone(), three.camera );
    var xToCheck = mouseClient.x - three.CANVAS_OFFSET_LEFT;
    var yToCheck = height - mouseClient.y + three.CANVAS_OFFSET_TOP;

    //---------------------
    // ray cast - if intersects object
    var intersects = raycaster.intersectObjects( three.objects, false );
    if ( intersects.length > 0 ) {
        for ( var ind2Obj in intersects ) { 
            var intObj = intersects[ind2Obj].object;
            if ( isPixelDrawnByObject(xToCheck, yToCheck, intObj)  ) {
                // select on map  
                selectObjectInScene(intObj);
                
                // select on map editor instance box
                three.uiMapEditor.selectObject(three.objSelected);

                // done
                return;
            }
        }
    }

    //---------------------
    // ray cast - if intersects floor and ctrl | cmd is pressed
    // create new object
    if (!ctrlDown) {
        return;
    }

    intersects = raycaster.intersectObjects( [three.grid], false );

    if ( intersects.length > 0 ) {
        // reposition selected item 
        var p = intersects[0].point;
        snapToGrid(p);
        if ( three.objSelected !== null ) {
            // duplicate selected
            if ( shiftDown ) {
                three.uiMapEditor.addInstance(p.x, three.objSelected.position.y, p.z);
            }
            // move selected
            else {
                three.objSelected.position.set(p.x, three.objSelected.position.y, p.z);
            }
        }
    }
}

function animateHandlePlayerMovement(timestep) {
    var pPos = three.player.position;
    var tCtrl = three.controls;
    var pl = three.player;

    var direction = new THREE.Vector3();
    if(pl.mDir1 === true) {
        direction.add(new THREE.Vector3(0, 0, -1));
        three.animInfo.lookAngle = Math.PI;
    }
    else if(pl.mDir2 === true) {
        direction.add(new THREE.Vector3(0, 0, 1));
        three.animInfo.lookAngle = 0.0;
    }   

    if(pl.mDir3 === true) {
        direction.add(new THREE.Vector3(-1, 0, 0));
        three.animInfo.lookAngle = Math.PI/2;
    }   
    else if(pl.mDir4 === true) {
        direction.add(new THREE.Vector3(1, 0, 0));
        three.animInfo.lookAngle = -Math.PI/2;
    }   

    if (shiftDown) {
        three.controls.userPanSpeed = timestep * 0.06 * 4;
    }
    else {
        three.controls.userPanSpeed = timestep * 0.06 * 5;
    }

    var diff = tCtrl.pan(direction);
    pPos.add( diff );

    if (pl.mDir1 || pl.mDir2 || pl.mDir3 || pl.mDir4) {
        three.animInfo.state = 'walkN';
    }
    else {
        three.animInfo.state = 'standN';
    }
}

function animate(lastTime, three){
    // ------------------------
    // get time difference
    var date = new Date();
    var time = date.getTime();
    timeDiff += time - lastTime;
    lastTime = time;

    three.controls.update();

    // ------------------------
    // process mouse events
    if ( hasMouseMoveEvent ) {
        animateHandleMouseMove();
    }

    if ( hasMouseClickEvent ) {
        animateHandleMouseClick();
    }

    // ------------------------
    // set character facing
    
    // get camera position
    var norm = three.camera.position.clone().setY(0);

    // set facing in sprite
    
    // player first
    var animInfo = three.animInfo;
    var rcos = animInfo.lookAngle;
    var rows = animInfo.rows;
    var tex = animInfo.tex;
    tex.offset.y = animInfo.animations[animInfo.state][1] / rows;
    tex.offset.y += +( ( rcos > -Math.PI/4 ) && ( rcos <= Math.PI/4 ) ) * 1 / rows +
        ( ( rcos > Math.PI/4 ) && ( rcos <= Math.PI*3/4 ) ) * 2 / rows +
        ( rcos > Math.PI*3/4 || rcos <= -Math.PI*3/4 ) * 3 / rows;

    // the rest
    var i = 0;
    for (i = 0; i < three.facingObjList.length; i++) {
        var obj = three.facingObjList[i];
        tex = three.loadedTextures[ obj.id ];
        animInfo = three.animationObjList[ obj.id ];
        var ref = obj.position; 
        rcos = Math.atan2( norm.x - ref.x, norm.z - ref.z );
        rows = animInfo.rows;
        tex.offset.y = animInfo.animations[animInfo.state][1] / rows;
        tex.offset.y += +( ( rcos > -Math.PI/4 ) && ( rcos <= Math.PI/4 ) ) * 1 / rows +
            ( ( rcos > Math.PI/4 ) && ( rcos <= Math.PI*3/4 ) ) * 2 / rows +
            ( rcos > Math.PI*3/4 || rcos <= -Math.PI*3/4 ) * 3 / rows;
    }

    // ------------------------
    // camera position (almost)
    var overThere = three.camera.position.clone().setY(64);

    // mesh facing towards camera projection
    for (i = 0; i < three.objects.length; i++) {
        three.objects[i].lookAt( overThere );
    }

    // ------------------------
    // reorder objects
    //three.objects = sort(three.objects);

    // ------------------------
    // animate movement
    animateHandlePlayerMovement(timeDiff);

    // ------------------------
    // animate sprites
    if (timeDiff >= animationTime) {
        timeDiff = 0;
        for(var objRef in three.animationObjList) {
            animInfo = three.animationObjList[objRef];
            animInfo.tex.offset.x = animInfo.currentFrame / animInfo.columns;
            animInfo.currentFrame += 1;
            if (animInfo.currentFrame >= animInfo.animations[animInfo.state][2]) {
                animInfo.currentFrame = 0;
            }
        }
    }

    // ------------------------
    // update map editor stuff
    // TODO: should i keep this here?
    if ( three.objBoundingBox ) {
        three.objBoundingBox.update();
    }
    if ( three.objBoundingBoxSel ) {
        three.objBoundingBoxSel.update();
    }
    if ( three.objCollisionBox ) {
        three.objCollisionBox.update();
    }
    if ( three.objCollisionBoxSel ) {
        three.objCollisionBoxSel.update();
    }

    // ------------------------
    // draw
    three.renderer.clear(); 
    three.renderer.render(three.scene, three.camera);
    three.renderer.render(three.uiScene, three.uiCamera);

    // ------------------------
    // update stats
    stats.update();

    // ------------------------
    // request new frame
    requestAnimFrame(function(){
        animate(lastTime, three);
    });
}

function loadMap( url ) {
    // get json obj
    // assign to csMap

    // loadMapResources
        
    // create scene objects
}

function loadMapResources( map, onLoad ) {
    var count = 0;
    var totalCount = 0;

    function showProgress(file, img) {
        return function() {
            three.preLoadedImages[ file ] = { 'img': img, 'w': img.width, 'h':img.height };
            // TODO: make visual
            console.log("Loaded image " + (++count) + "/" + totalCount );
            if ( count === totalCount ) {
                if ( onLoad ) onLoad();
            }
        };
    }

    function scheduleImage(file) {
        // has already been scheduled
        if ( three.preLoadedImages[ file ] ) {
            ++count;
        }
        // new entry
        else {  
            var image = new Image();
            image.onload = showProgress(file, image);
            image.src = file;
            three.preLoadedImages[ file ] = { 'img': null, 'w': 0, 'h': 0 };
        }
    }

    var ind, mapEntry, texDetails;
    var it, eqIt, itemName, itemEntry, tex;

    // count map textures
    for ( ind = 0; ind < map.length; ind++ ) {
        mapEntry = map[ind];
        texDetails = csTextures[mapEntry.texRef];

        totalCount++;

        if ( mapEntry.name == "StartPosition" ) {
            // TODO: load character sheet
            var characterSheet = csR.csCreateCharacter();
            three.characterSheet = characterSheet;

            // load item images
            for( it = 0; it < characterSheet.Inventory.length; it++ ) {
                itemName = three.characterSheet.Inventory[it];
                itemEntry = Items[itemName];
                if ( itemEntry.Map ) {
                    totalCount++;
                }
            }

            // load eqItem images
            for( eqIt in characterSheet.EqBodyParts ) {
                if ( characterSheet.EqBodyParts.hasOwnProperty( eqIt ) &&
                    three.characterSheet.EqBodyParts[ eqIt ] ) {
                    itemName = three.characterSheet.EqBodyParts[eqIt];
                    itemEntry = Items[itemName];
                    if ( itemEntry.Map ) {
                        totalCount++;
                    }
                }
            }
        }
    }

    // load map entries
    for ( ind = 0; ind < map.length; ind++ ) {
        mapEntry = map[ind];
        texDetails = csTextures[mapEntry.texRef];

         scheduleImage( texDetails.file );       

        // load character sheets + item images
        if ( mapEntry.name === "StartPosition" ) {
            // load item images
            for( it = 0; it < three.characterSheet.Inventory.length; it++ ) {
                itemName = three.characterSheet.Inventory[it];
                itemEntry = Items[itemName];
                if ( itemEntry.Map ) {
                    tex = csTextures[ itemEntry.Map ]; 
                    scheduleImage( tex.file );       
                }
            }

            // load eqItem images
            for( eqIt in three.characterSheet.EqBodyParts ) {
                if ( three.characterSheet.EqBodyParts.hasOwnProperty( eqIt ) && 
                    three.characterSheet.EqBodyParts[ eqIt ] ) {
                    itemName = three.characterSheet.EqBodyParts[eqIt];
                    itemEntry = Items[itemName];
                    if ( itemEntry.Map ) {
                        tex = csTextures[ itemEntry.Map ]; 
                        scheduleImage( tex.file );       
                    }
                }
            }
        }
    }
}

function createObjectTexture( mapEntry ) {
    // set up a canvas to create new textures
    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');

    // get obj basic tex
    var texDetails = csTextures[mapEntry.texRef];
    var texData = three.preLoadedImages[ texDetails.file ];
    canvas.width = texData.w;
    canvas.height = texData.h;

    // canvas contents will be used for a texture
    var texture = new THREE.Texture(canvas);

    // manipulate image using background
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.rect(0, 0, canvas.width, canvas.height);
    
    // draw on canvas
    context.drawImage(texData.img, 0, 0);

    // walk through obj equipped inv
    // TODO: do this for every character
    if ( mapEntry.name === "StartPosition" ) {
        var drawOrderList = [ "FEET", "LEGS", "TORSO", "BELT", "HEAD", "HANDLF", "HANDRF", "NECK", "WEAPONR", "WEAPONL" ];

        // get eq obj tex
        for ( var ind = 0; ind < drawOrderList.length; ind++ ) {
            var ref = three.characterSheet.EqBodyParts[ drawOrderList[ ind ] ];
            if ( ref ) {
                var itemTexDetails = csTextures[ Items[ ref ].Map ];
                texData = three.preLoadedImages[ itemTexDetails.file ];
                // draw on canvas
                context.drawImage(texData.img, 0, 0);
            }
        }    
    }

    if( texDetails.hasOwnProperty('rows') ) {
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set( 1/texDetails.cols, 1/texDetails.rows );
    }

    // schedule for update
    texture.needsUpdate = true;

    // return texture
    return texture;
}

function updateObjectTexture( obj ) {
    var mapEntry = three.objectProperties[ obj.id ];
    var tex = createObjectTexture( mapEntry );
    obj.material.map = tex; 
    three.animationObjList[obj.id].tex = tex; 
    obj.material.needsUpdate = true;
}

function createGameObject( mapEntry ) {
    // get texture details
    var texDetails = csTextures[mapEntry.texRef];
    
    // load texture and set wrap
    var texture = createObjectTexture( mapEntry );

    // create material
    var material = null;
    if( mapEntry.inGameType === "ground" ) {
        material = new THREE.MeshLambertMaterial( { map: texture, wireframe: false } );
    }
    else {
        material = new THREE.MeshLambertMaterial( { map: texture, transparent: true, depthWrite: true, alphaTest: 0 } );
    }

    // create mesh
    var mesh = null;
    if( mapEntry.type === 'plane' ) {
        mesh = new THREE.Mesh( new THREE.PlaneGeometry( mapEntry.size[0], mapEntry.size[1] ), material );
        mesh.position.set( mapEntry.pos[0], mapEntry.pos[1], mapEntry.pos[2] );
    }

    // apply rotation
    mesh.rotation.x = mapEntry.rot[0];
    mesh.rotation.y = mapEntry.rot[1];
    mesh.rotation.z = mapEntry.rot[2];

    // add to scene
    three.scene.add( mesh );

    // add to properties list
    three.objectProperties[mesh.id] = mapEntry;

    // add to apppropriate lists 
    if ( mapEntry.inGameType === "ground" ) {
        // TODO: replace with list
        three.grid = mesh;
    }
    else if ( mapEntry.inGameType === "character" ) {
        var animInfo =  { 
            'tex': texture,
            'state': 'standN',
            'lookAngle': 0,
            'currentFrame': 0, 
            'animations': texDetails.animations, 
            'columns': texDetails.cols,
            'rows': texDetails.rows,
        };

        three.animationObjList[mesh.id] = animInfo;
        three.aiObjList.push( mesh );
        three.objects.push( mesh );
        if ( mapEntry.name === 'StartPosition' ) {
            three.player = mesh;
            three.animInfo = animInfo;
        }
        else {
            three.facingObjList.push( mesh );
        }
    }
    else if ( mapEntry.inGameType === "object" ) {
        three.objects.push( mesh );
    }

    // store texture for further reference
    three.loadedTextures[mesh.id] = texture;

    return mesh;
}

function createAllGameObjects() {
    for( var entry=0; entry<csMap.length; entry++ ) {
        // create from entry
        var gObj = createGameObject( csMap[entry] );
    }

    three.player.mDir1 = false; 
    three.player.mDir2 = false; 
    three.player.mDir3 = false; 
    three.player.mDir4 = false; 

    three.camera.lookAt( three.player );

    var uiMapEditor = createMapEditorUI(three.uiScene);
    three.uiMapEditor = uiMapEditor;
    hideAllGameObjects();
    var gui = createMainScreen( three.uiScene );
    gui.addToScene();
    three.uiMainScreen = gui;

    animate(0, three, this);
}

function hideAllGameObjects() {
    for( var entry=0; entry<three.objects.length; entry++ ) {
        three.scene.remove( three.objects[entry] );
    }

    three.scene.remove( three.grid );
    if (three.uiMapEditor) {
        three.uiMapEditor.removeFromScene();
    }
}

function showAllGameObjects() {
    for( var entry=0; entry<three.objects.length; entry++ ) {
        three.scene.add( three.objects[entry] );
    }

    three.scene.add( three.grid );
    if (three.uiMapEditor) {
        three.uiMapEditor.addToScene();
    }

}


function removeGameObject( object ) {
    if ( object === null || typeof object == 'undefined' ) {
        return;
    }

    // select none on map  
    selectObjectInScene(null);
        
    // select none on map editor instance box
    three.uiMapEditor.selectObject(null);

    // remove from scene
    three.scene.remove( object );

    var mapEntry = three.objectProperties[object.id];
    var idx = -1;

    // remove from apppropriate lists 
    if ( mapEntry.inGameType === "ground" ) {
        // TODO: replace with list
        three.grid = null;
    }
    else if ( mapEntry.inGameType === "character" ) {
        idx = three.animationObjList.indexOf( object );
        if (idx > -1) {
            three.animationObjList.splice( idx, 1 );
        }

        idx = three.aiObjList.indexOf( object );
        if (idx > -1) {
            three.aiObjList.splice( idx, 1 );
        }

        idx = three.facingObjList.indexOf( object );
        if (idx > -1) {
            three.facingObjList.splice( idx, 1 );
        }

        idx = three.objects.indexOf( object );
        if (idx > -1) {
            three.objects.splice( idx, 1 );
        }
    }
    else if ( mapEntry.inGameType === "object" ) {
        idx = three.objects.indexOf( object );
        if (idx > -1) {
            three.objects.splice( idx, 1 );
        }
    }

    // remove from properties list
    delete three.objectProperties[object.id];

    return object;
}

// init stuff
// ------------------------------------
function init(){
    var objects = [];

    var CANVAS_WIDTH = 1200;
    var CANVAS_HEIGHT = 680;

    // renderer
    var renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true, preserveDrawingBuffer: true } );
    renderer.setClearColor( 0x000000, 1 );
    renderer.sortObjects = true;
    renderer.setSize( CANVAS_WIDTH, CANVAS_HEIGHT);
    renderer.autoClear = false;

    var container = document.getElementById("container");
    container.appendChild( renderer.domElement );

    var ppsRenderTarget = new THREE.WebGLRenderTarget( CANVAS_WIDTH, CANVAS_HEIGHT, { minFilter: THREE.LinearFilter, magFilter: THREE.NearestFilter, format: THREE.RGBFormat } );

    var CANVAS_OFFSET_LEFT = container.offsetLeft;
    var CANVAS_OFFSET_TOP = container.offsetTop;

    // camera
    var camera = new THREE.PerspectiveCamera(60, CANVAS_WIDTH / CANVAS_HEIGHT, 1, 2000);
    camera.position.setY(225);
    camera.position.setZ(0);

    var uiCamera = new THREE.OrthographicCamera(0, CANVAS_WIDTH, CANVAS_HEIGHT, 0, -10, 10);

    // controls
    var controls = new THREE.OrbitControls( camera );
    controls.minDistance = 0;
    controls.maxDistance = 1000;
    controls.minPolarAngle = -Math.PI/3;
    controls.maxPolarAngle = Math.PI;
    controls.userPanSpeed = 10.0;

    // scene - lights
    var scene = new THREE.Scene();
    scene.add( new THREE.AmbientLight( 0xa0a0a0 ) );

    var uiScene = new THREE.Scene();

    var ppsScene = new THREE.Scene();
    ppsScene.add( new THREE.AmbientLight( 0xa0a0a0 ) );

    var light = new THREE.SpotLight( 0xa0a0a0, 1.2 );
    light.position.set( 400, 150, 600 );
    light.castShadow = true;
    scene.add( light );

    var ppsLight = new THREE.SpotLight( 0xa0a0a0, 1.2 );
    ppsLight.position.set( 400, 150, 600 );
    ppsLight.castShadow = true;
    ppsScene.add( ppsLight );

    //-------------------------------------------
    // DEBUG 
    
    //cameraHelper = new THREE.CameraHelper( camera );
    //scene.add( cameraHelper );

    // grid
    var grid = new THREE.GridHelper(512, 64);
    scene.add(grid);

    //-------------------------------------------

    // grid selector
    var geometry = new THREE.CubeGeometry( 64, 64, 64 );
    var gridSelectorMaterial = new THREE.MeshBasicMaterial( {color: 0x00ff00, wireframe: true} );
    var gridSelector = new THREE.Mesh( geometry, gridSelectorMaterial );
    scene.add( gridSelector );
    gridSelector.position.set(5, 32, 5);

    // create a projector to cast rays
    projector = new THREE.Projector();

    // objList = interactive object (collision)
    // floorList = floor/ground/stairs/hills
    var objectProperties = {};
    var animationObjList = [];
    var aiObjList = [];
    var facingObjList = [];

    // create wrapper object that contains three.js objects
    three = {
        // canvas positional and size 
        CANVAS_WIDTH: CANVAS_WIDTH,
        CANVAS_HEIGHT: CANVAS_HEIGHT,
        CANVAS_OFFSET_LEFT: CANVAS_OFFSET_LEFT,
        CANVAS_OFFSET_TOP: CANVAS_OFFSET_TOP,

        // in-game objects under focus  
        objUnderMouse: null,
        objSelected: null,
        objBoundingBox: null,
        objCollisionBox: null,

        // render stuff
        renderer: renderer,
        ppsRenderTarget: ppsRenderTarget,
        camera: camera,
        uiCamera: uiCamera,
        scene: scene,
        uiScene: uiScene,
        ppsScene: ppsScene,

        // controls
        controls: controls,

        // character objec + info
        player: null,
        animInfo: null,

        // all interactive objects
        objects: objects,
        aiObjList: aiObjList,
        animationObjList: animationObjList,
        facingObjList: facingObjList,
        buildingsList: null,
        grid: null,                    // TODO: replace with list
        gridSelector: gridSelector,
        loadedTextures: {},
        preLoadedImages: {},

        // object properties script/rules
        objectProperties: objectProperties,

        // player stats
        characterSheet: null,
        characterSheetDraft: null,

        // ui screens
        uiMainScreen: null,
        uiMapEditor: null,
        uiInGame: null
    };

    // pre load stuff
    loadMapResources( csMap, createAllGameObjects );

    // bind mouse and keyboard
    renderer.domElement.addEventListener( 'mousemove', onDocumentMouseMove, false );
    renderer.domElement.addEventListener( 'mousedown', onDocumentMouseDown, false );
    renderer.domElement.addEventListener( 'mouseup', onDocumentMouseUp, false );
    onkeyup = onkeydown = handleKeyEvents;

    // stats
    stats = new Stats();
    container.appendChild( stats.domElement );
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '630px';
    stats.domElement.style.left = '0px';
}
