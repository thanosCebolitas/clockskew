var Items = {
    //=================================================
    // Weapons
    'Wrong Longsword' : {
        'Type': 'Longsword',
        'TexType': 'WEAPONR',
        'Damage': {'Dice': [6, 4], 'Fixed':1, 'Effects': ['Vorpal']},
        'AC': 0,
        'AttackRoll': 1,
        'Map': 'dagger',
        'Icon': 'swrd2',
        'Cost': 16330
    },
    //=================================================
    // Shields 
    'Wooden Shield +1' : {
        'Type': 'Shield',
        'TexType': 'WEAPONL',
        'Damage': {'Dice': [6], 'Fixed':1, 'Effects': []},
        'AC': 3, 
        'AttackRoll': 1,
        'Map': 'shield',
        'Icon': 'shld2',
        'Cost': 6330
    },
    //=================================================
    // Necklaces
    'Bronze Necklace' : {
        'Type': 'Necklace',
        'TexType': null,
        'Damage': {'Dice': [], 'Fixed':0, 'Effects': []},
        'AC': 0,
        'AttackRoll': 0,
        'Map': null,
        'Icon': 'nklc1',
        'Cost': 16330
    },       
    //=================================================
    // Rings 
    'Bronze Ring' : {
        'Type': 'Ring',
        'TexType': null,
        'Damage': {'Dice': [], 'Fixed':0, 'Effects': []},
        'AC': 0,
        'AttackRoll': 0,
        'Map': null,
        'Icon': 'nklc3',
        'Cost': 330
    },
    //=================================================
    // Gloves 
    'Metal Gloves' : {
        'Type': 'Gloves',
        'TexType': 'HANDS',
        'Damage': {'Dice': [], 'Fixed':0, 'Effects': []},
        'AC': 1,
        'AttackRoll': 0,
        'Map': 'gloves',
        'Icon': 'boot2',
        'Cost': 1330
    },
    //=================================================
    // Helmets
    'Metal Helmet' : {
        'Type': 'Helmet',
        'TexType': 'HEAD',
        'Damage': {'Dice': [], 'Fixed':0, 'Effects': []},
        'AC': 1,
        'AttackRoll': 0,
        'Map': 'helmet',
        'Icon': 'hlmt1',
        'Cost': 1330
    },
    'Hood' : {
        'Type': 'Helmet',
        'TexType': 'HEAD',
        'Damage': {'Dice': [], 'Fixed':0, 'Effects': []},
        'AC': 1,
        'AttackRoll': 0,
        'Map': 'hood',
        'Icon': 'hlmt1',
        'Cost': 1330
    },
    //=================================================
    // Belts 
    'Leather Belt' : {
        'Type': 'Belt',
        'TexType': 'BELT',
        'Damage': {'Dice': [], 'Fixed':0, 'Effects': []},
        'AC': 0,
        'AttackRoll': 0,
        'Map': 'belt',
        'Icon': 'hlmt2',
        'Cost': 30
    },       
    //=================================================
    // Armors (Torso) 
    'Plate Armor' : {
        'Type': 'Armor',
        'TexType': 'TORSO',
        'Damage': {'Dice': [], 'Fixed':0, 'Effects': []},
        'AC': 3,
        'AttackRoll': 1,
        'Map': 'plate',
        'Icon': 'shld3',
        'Cost': 6330
    },
    //=================================================
    // Pants 
    'Metal Pants' : {
        'Type': 'Pants',
        'TexType': 'LEGS',
        'Damage': {'Dice': [], 'Fixed':0, 'Effects': []},
        'AC': 1,
        'AttackRoll': 0,
        'Map': 'pants',
        'Icon': 'boot3',
        'Cost': 1330
    },
    //=================================================
    // Boots / Shoes
    'Metal Boots' : {
        'Type': 'Boots',
        'TexType': 'FEET',
        'Damage': {'Dice': [], 'Fixed':0, 'Effects': []},
        'AC': 1,
        'AttackRoll': 0,
        'Map': 'boots',
        'Icon': 'boot1',
        'Cost': 16330
    }       
};

