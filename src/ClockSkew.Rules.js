/*! The Ruleset Object
 * offers variable scope to protect the global namespace  */
var csR = function () {
    var that = {};

    // =============================================================
    // Private objects

    /*! Human traits */
    var Human = {
        'Skills'    :   function(level) { return level + 3; },
        'Feats' :   function() { return 1; },
        'Languages' :   function() { return 1; }
    };

    /*! Dwarf traits */
    var Dwarf = {
        'Con'   :   function() { return 2; },
        'Cha'   :   function() { return -2; },
        'Speed' :   function() { return -10; },
        'Feats' :   function() { return 'Darkvision'; }
    };

    /*! Peasant traits */
    var Peasant = {
        'HP'        :   4,
        'BAB'       :   function(level) { return level; },
        'Fortitude' :   function(level) { return ((level===0)?0:1+Math.round((level-1)/2)); },
        'Reflex'    :   function(level) { return Math.round(level/3); },
        'Will'      :   function(level) { return Math.round(level/3); },
        'Skills'    :   function(level) { return 4*level; },
        'Feats'     :   []
    };

    /*! Thief class traits */
    var Thief = {
        'HP'        :   6,
        'BAB'       :   function(level) { return level; },
        'Fortitude' :   function(level) { return Math.round(level/3); },
        'Reflex'    :   function(level) { return ((level===0)?0:1+Math.round((level-1)/2)); },
        'Will'      :   function(level) { return Math.round(level/3); },
        'Skills'    :   function(level, intel) { return ((level===0)?0:(8 + intel)*(level+3)); },
        'Feats'     :   []
    };

    /*! Barbarian class traits */
    var Barbarian = {
        'HP'        :   12,
        'BAB'       :   function(level) { return level; },
        'Fortitude' :   function(level) { return ((level===0)?0:1+Math.round((level-1)/2)); },
        'Reflex'    :   function(level) { return ((level===0)?0:1+Math.round((level-1)/2)); },
        'Will'      :   function(level) { return Math.round(level/3); },
        'Skills'    :   function(level, intel) { return ((level===0)?0:(4 + intel)*(level+3)); },
        'Feats'     :   ['Martial Weapon Proficiency', 'LAWP', 'MAWP', 'SP', 'Fast Movement', 'Rage']
    };

    /*! Fighter class traits */
    var Fighter = {
        'HP'        :   10,
        'BAB'       :   function(level) { return level; },
        'Fortitude' :   function(level) { return ((level===0)?0:1+Math.round((level-1)/2)); },
        'Reflex'    :   function(level) { return Math.round(level/3); },
        'Will'      :   function(level) { return Math.round(level/3); },
        'Skills'    :   function(level, intel) { return ((level === 0)?0:(2 + intel)*(level+3)); },
        'Feats'     :   ['Martial Weapon Proficiency', 'LAWP', 'MAWP', 'SP'],
        'Feats$'    :   function(level) { return ((level===0)?0:1+Math.round((level-1)/2)); }
    };

    /*! This object contains the character sheet data that describe a players state. */
    var startingCharacterProperties = {
        // Personal
        'Name'          : {writable: true, configurable: true, enumerable: true, value:   'name'},
        'Age'           : {writable: true, configurable: true, enumerable: true, value:   [16, 16]},
        'Gender'        : {writable: true, configurable: true, enumerable: true, value:   ['Female', 'Female']},
        'Race'          : {writable: true, configurable: true, enumerable: true, value:   ['Human','Human']},
        'Class'         : {writable: true, configurable: true, enumerable: true, value:  [[0, 0, 0], [0, 0, 0]]},
        'Size'          : {writable: true, configurable: true, enumerable: true, value:  [0, 0]},
        'Languages' : {writable: true, configurable: true, enumerable: true, value:  [[], []]},
        'Alignment' : {writable: true, configurable: true, enumerable: true, value:  [['LE'], ['LE']]},
        'XP'        : {writable: true, configurable: true, enumerable: true, value:  [0, 0]},
        // Attributes
        'Str'       : {writable: true, configurable: true, enumerable: true, value:  [8, 8]},
        'Dex'       : {writable: true, configurable: true, enumerable: true, value:  [8, 8]},
        'Con'       : {writable: true, configurable: true, enumerable: true, value:  [8, 8]},
        'Int'       : {writable: true, configurable: true, enumerable: true, value:  [8, 8]},
        'Wis'       : {writable: true, configurable: true, enumerable: true, value:  [8, 8]},
        'Cha'           : {writable: true, configurable: true, enumerable: true, value:  [8, 8]},
        // Stats
        'HP'            : {writable: true, configurable: true, enumerable: true, value:  [0, 0]},
        'Hardness'      : {writable: true, configurable: true, enumerable: true, value:  [0, 0]},
        'BAB'           : {writable: true, configurable: true, enumerable: true, value:  [0, 0]},
        'Melee'     : {writable: true, configurable: true, enumerable: true, value:  [0, 0]},
        'Ranged'    : {writable: true, configurable: true, enumerable: true, value:  [0, 0]},
        'AC'            : {writable: true, configurable: true, enumerable: true, value:  [10, 10]},
        'Speed'         : {writable: true, configurable: true, enumerable: true, value:  [30, 30]},
        'Fortitude' : {writable: true, configurable: true, enumerable: true, value:  [0, 0]},
        'Reflex'    : {writable: true, configurable: true, enumerable: true, value:  [0, 0]},
        'Will'          : {writable: true, configurable: true, enumerable: true, value:  [0, 0]},
        'Initiative'    : {writable: true, configurable: true, enumerable: true, value:  [0, 0]},
        // Skills
        'Skills'    : {writable: true, configurable: true, enumerable: true, value:  null},
        // Feats
        'Feats'     : {writable: true, configurable: true, enumerable: true, value:  null},
        // Spells
        'Spells'    : {writable: true, configurable: true, enumerable: true, value:  [[], []]},
        // Inventory
        'Inventory' : {writable: true, configurable: true, enumerable: true, value:  []},
        // "Equippable" Inventory
        'EqBodyParts'   : {writable: true, configurable: true, enumerable: true, value:  {
            FEET: null,
            LEGS: null,
            TORSO: 'Plate Armor',
            BELT: null,
            HEAD: 'Metal Helmet',
            HANDLF: null,
            HANDRF: null,
            NECK: null,
            WEAPONR: null,
            WEAPONL: null
        }},
        // "Temporary" effects hp/abilities/level drain/boosts etc
        'Effects'       : {writable: true, configurable: true, enumerable: true, value:      []},
        // points not spent
        'remainingFeats' : {writable: true, configurable: true, enumerable: true, value: 2},
        'remainingSkills' : {writable: true, configurable: true, enumerable: true, value: 0},
        'remainingAttributes' : {writable: true, configurable: true, enumerable: true, value: 24},
        'remainingSpells' : {writable: true, configurable: true, enumerable: true, value: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}
    };


    /*! This object encloses some util functions and is used as the characterSheet's prototype.
     * Check out the Object.create() invocation in the csCreateCharacterSheet maker function.  */ 
    var characterUtilFunctions = {
        /*! This function updates the skill points the player has not spent yet.
         * @param playerClass One of the strings 'Thief', 'Barbarian' or 'Fighter'
         * @param v the current intelligence value
         * @param p the previous intelligence value */
        updateRemSkills : function(playerClass, v, p) {
            var newSheet = this; 
            
            if(playerClass === 'Thief') {
                // count skills
                newSheet.remainingSkills -= Thief.Skills(newSheet.Class[1][2], p) - Thief.Skills(newSheet.Class[1][2] - 1, p);
                newSheet.remainingSkills += Thief.Skills(newSheet.Class[1][2], v) - Thief.Skills(newSheet.Class[1][2] - 1, v);
            }
            else if(playerClass === 'Barbarian') {
                // count skills
                newSheet.remainingSkills -= Barbarian.Skills(newSheet.Class[1][1], p) - Barbarian.Skills(newSheet.Class[1][1] - 1, p);
                newSheet.remainingSkills += Barbarian.Skills(newSheet.Class[1][1], v) - Barbarian.Skills(newSheet.Class[1][1] - 1, v);
            }
            else if(playerClass === 'Fighter') {
                // count skills
                newSheet.remainingSkills -= Fighter.Skills(newSheet.Class[1][0], p) - Fighter.Skills(newSheet.Class[1][0] - 1, p);
                newSheet.remainingSkills += Fighter.Skills(newSheet.Class[1][0], v) - Fighter.Skills(newSheet.Class[1][0] - 1, v);
            }

        },
        /*! The function applies the level up rules to the character sheet,
         * according to the string defined in playerClass.  ('Thief' | 'Barbarian' | 'Fighter') */
        addClassToCharacterSheet : function(playerClass) {
            var newSheet = this; 
            var iMod = Math.floor((newSheet.Int[1]-10)/2);

            if(playerClass === 'Thief') {
               newSheet.Class[1][2]++;
                // count skills
                newSheet.remainingSkills += Thief.Skills(newSheet.Class[1][2], iMod) - Thief.Skills(newSheet.Class[1][2] - 1, iMod);
                // hp
                newSheet.HP[1] += ((newSheet.Class[1][2]===1)? Thief.HP : Math.floor((Math.random() * Thief.HP) + 1)) + Math.floor((newSheet.Con[1]-10)/2)*2;
            }
            else if(playerClass === 'Barbarian') {
               newSheet.Class[1][1]++;
                // count skills
                newSheet.remainingSkills += Barbarian.Skills(newSheet.Class[1][1], iMod) - Barbarian.Skills(newSheet.Class[1][1] - 1, iMod);
                // hp
                newSheet.HP[1] += ((newSheet.Class[1][1]===1)? Barbarian.HP : Math.floor((Math.random() * Barbarian.HP) + 1)) + Math.floor((newSheet.Con[1]-10)/2)*2;
            }
            else if(playerClass === 'Fighter') {
               newSheet.Class[1][0]++;
                // count skills
                newSheet.remainingSkills += Fighter.Skills(newSheet.Class[1][0], iMod) - Fighter.Skills(newSheet.Class[1][0] - 1, iMod);
                // count feats
                newSheet.remainingFeats += Fighter['Feats$'](newSheet.Class[1][0]) - Fighter['Feats$'](newSheet.Class[1][0] - 1);
                // hp
                newSheet.HP[1] += ((newSheet.Class[1][0]===1)? Fighter.HP : Math.floor((Math.random() * Fighter.HP) + 1)) + Math.floor((newSheet.Con[1]-10)/2)*2;
            }
            // Level
            var lvl = 0;
            for(var index=0; index<newSheet.Class[1].length; index++) {
                lvl += newSheet.Class[1][index]; 
            }
            if(lvl % 4 === 0) {
                newSheet.remainingAttributes++;
                console.log(newSheet.remainingAttributes);
            }
            else if(lvl % 3 === 0) {
                newSheet.remainingFeats++;
            }

            // BAB
            newSheet.BAB[1] = Fighter.BAB(newSheet.Class[1][0]) + Barbarian.BAB(newSheet.Class[1][1]) + Thief.BAB(newSheet.Class[1][2]);
            // Melee
            newSheet.Melee[1] = newSheet.BAB[1] + Math.floor((newSheet.Str[1] - 10) * 0.5); 
            // Ranged
            newSheet.Ranged[1] = newSheet.BAB[1] + Math.floor((newSheet.Dex[1] - 10) * 0.5); 

            // ac
            newSheet.AC[1] = 10 + Math.floor((newSheet.Dex[1] - 10) * 0.5);

            // Init
            newSheet.Initiative[1] = Math.floor((newSheet.Dex[1] - 10) * 0.5) + ((newSheet.Feats['Improved Initiative'].isSelected === true)?4:0);

            // fort/ref/will
            newSheet.Fortitude[1] = Fighter.Fortitude(newSheet.Class[1][0]) + Barbarian.Fortitude(newSheet.Class[1][1]) + Thief.Fortitude(newSheet.Class[1][2]);
            newSheet.Reflex[1] = Fighter.Reflex(newSheet.Class[1][0]) + Barbarian.Reflex(newSheet.Class[1][1]) + Thief.Reflex(newSheet.Class[1][2]);
            newSheet.Will[1] = Fighter.Will(newSheet.Class[1][0]) + Barbarian.Will(newSheet.Class[1][1]) + Thief.Will(newSheet.Class[1][2]);

            newSheet.Fortitude[1] += Math.floor((newSheet.Con[1] - 10) * 0.5) + ((newSheet.Feats['Great Fortitude'].isSelected)?4:0);
            newSheet.Reflex[1] += Math.floor((newSheet.Dex[1] - 10) * 0.5) + ((newSheet.Feats['Lightning Reflexes'].isSelected)?4:0);
            newSheet.Will[1] += Math.floor((newSheet.Wis[1] - 10) * 0.5) + ((newSheet.Feats['Iron Will'].isSelected)?4:0);
            return newSheet;
        }
    };




    // =============================================================
    // Private functions 


    /*! Maker Function: Creates the data structure containing the Skill tree information.
     * @return the Skills object */
    var csCreateSkills = function() {
        // Skill-list
        var that = {
            // Name                              Current,   Points, Score,  Armor Penalty,  Trained only,   Synergy
            'Appraise'                      :   [0, 0,     'Int',  false,          false,          ['Craft']       ],
            'Balance'                       :   [0, 0,     'Dex',  true,           false,          ['Tumble']       ],
            'Bluff'                         :   [0, 0,     'Cha',  false,          false,          []              ],
            'Climb'                         :   [0, 0,     'Str',  true,           false,          []              ],
            'Concentration'                 :   [0, 0,     'Con',  false,          false,          []              ],
            'Craft'                         :   [0, 0,     'Int',  false,          true,           []              ],
            'Decipher Script'               :   [0, 0,     'Int',  false,          true,           []              ],
            'Diplomacy'                     :   [0, 0,     'Cha',  false,          false,          ['Bluff', 'Knowledge (Nobility and royalty)', 'Sense Motive']   ],
            'Disable Device'                :   [0, 0,     'Int',  false,          true,           []              ],
            'Disguise'                      :   [0, 0,     'Cha',  false,          false,          ['Bluff']       ],
            'Forgery'                       :   [0, 0,     'Int',  false,          true,           []              ],
            'Gather Information'            :   [0, 0,     'Cha',  false,          false,          ['Knowledge (Local)']   ],
            'Handle Animal'                 :   [0, 0,     'Cha',  false,          false,          []              ],
            'Heal'                          :   [0, 0,     'Wis',  false,          true,           []              ],
            'Hide'                          :   [0, 0,     'Dex',  true,           false,          []              ],
            'Intimidate'                    :   [0, 0,     'Cha',  false,          false,          ['Bluff']       ],
            'Jump'                          :   [0, 0,     'Str',  true,           false,          ['Tumble', ]              ],
            'Knowledge (Arcana)'            :   [0, 0,     'Int',  false,          false,          []              ],
            'Knowledge (Engineering)'       :   [0, 0,     'Int',  false,          false,          []              ],
            'Knowledge (Geography)'         :   [0, 0,     'Int',  false,          false,          []              ],
            'Knowledge (History)'           :   [0, 0,     'Int',  false,          false,          []              ],
            'Knowledge (Local)'             :   [0, 0,     'Int',  false,          false,          []              ],
            'Knowledge (Nature)'            :   [0, 0,     'Int',  false,          false,          ['Survival']              ],
            'Knowledge (Nobility)'          :   [0, 0,     'Int',  false,          false,          []              ],
            'Knowledge (Religion)'          :   [0, 0,     'Int',  false,          false,          []              ],
            'Listen'                        :   [0, 0,     'Wis',  false,          false,          []              ],
            'Move Silently'                 :   [0, 0,     'Dex',  true,           false,          []              ],
            'Open Lock'                     :   [0, 0,     'Dex',  false,          true,           []              ],
            'Perform'                       :   [0, 0,     'Cha',  false,          false,          []              ],
            'Ride'                          :   [0, 0,     'Dex',  true,           false,          ['Handle Animal']              ],
            'Sail'                          :   [0, 0,     'Wis',  true,           true,           []              ],
            'Search'                        :   [0, 0,     'Int',  false,          false,          []              ],
            'Sense Motive'                  :   [0, 0,     'Wis',  false,          false,          []              ],
            'Sleight Of Hand'               :   [0, 0,     'Dex',  false,          false,          ['Bluff']       ],
            'Speak Language'                :   [0, 0,     'Int',  false,          true,           []              ],
            'Spellcraft'                    :   [0, 0,     'Int',  false,          true,           ['Knowledge (Arcana)']              ],
            'Spot'                          :   [0, 0,     'Wis',  false,          false,          []              ],
            'Survival'                      :   [0, 0,     'Wis',  false,          false,          ['Knowledge (Geography)', 'Knowledge (Nature)', 'Search']              ],
            'Swim'                          :   [0, 0,     'Str',  true,           false,          []              ],
            'Tumble'                        :   [0, 0,     'Dex',  true,           true,           ['Jump']              ],
            'Use Magic Device'              :   [0, 0,     'Cha',  false,          true,           ['Decipher Script', 'Spellcraft']      ],
            'Use Rope'                      :   [0, 0,     'Dex',  true,           false,          []               ]
        };

        return that;
    };

    /*! Maker Function: Creates the data structure containing the Feats tree information.
     * @return The Feats object */
    var csCreateFeats = function() {
        // Feat-list
        var that = {
            'Acrobatic'     : { 'isSelected': false, 'Skills' : { 'Jump' : 2, 'Tumble' : 2 } } ,
            'Agile'         : { 'isSelected': false, 'Skills' : { 'Balance' : 2, 'Tumble' : 2 } },
            'Alertness'     : { 'Skills' : { 'Listen' : 2, 'Spot' : 2 } },
            'Animal Affinity'       : { 'isSelected': false, 'Skills' : { 'Handle Animal' : 2, 'Ride' : 2 } },
            'Armor Proficiency (Heavy)'    : { 'isSelected': false },
            'Armor Proficiency (Light)'    : { 'isSelected': false },
            'Armor Proficiency (Medium)'    : { 'isSelected': false },
            'Athletic'      : { 'isSelected': false, 'Skills' : { 'Climb' : 2, 'Swim' : 2 } },
    //        AugmentSummoning    :   { 'isSelected': false},
            'Blind Fight' :   { 'isSelected': false},
    //        BrewPotion :   { 'isSelected': false},
            'Cleave' :   { 'isSelected': false},
    //        CombatCasting :   { 'isSelected': false},
            'Combat Expertise' :   { 'isSelected': false},
            'Combat Reflexes' :   { 'isSelected': false},
    //        CraftMagicArmsAndArmor :   { 'isSelected': false},
    //        CraftRod :   { 'isSelected': false},
    //        CraftStaff :   { 'isSelected': false},
    //        CraftWand :   { 'isSelected': false},
    //        CraftWondrousItem :   { 'isSelected': false},
            'Deceitful'     : { 'isSelected': false, 'Skills' : { 'Disguise' : 2, 'Forgery' : 2 } },
            'Deflect Arrows' :   { 'isSelected': false},
            'Deft Hands'        : { 'isSelected': false, 'Skills' : { 'Sleight of Hand' : 2, 'Use Rope' : 2 } },
            'Die Hard' :   { 'isSelected': false},
            'Diligent'      : { 'isSelected': false, 'Skills' : { 'Appraise' : 2, 'Decipher Script' : 2 } },
            'Dodge' :   { 'isSelected': false},
    //        EmpowerSpell :   { 'isSelected': false},
            'Endurance' :   { 'isSelected': false},
    //        EnlargeSpell :   { 'isSelected': false},
    //        EschewMaterials :   { 'isSelected': false},
            'Exotic Weapon Proficiency' :   { 'isSelected': false},
    //        ExtendSpell :   { 'isSelected': false},
    //        ExtraTurning :   { 'isSelected': false},
            'Far Shot' :   { 'isSelected': false},
    //        ForgeRing :   { 'isSelected': false},
            'Great Cleave' :   { 'isSelected': false},
            'Great Fortitude' :   { 'isSelected': false},
    //        GreaterSpellFocus :   { 'isSelected': false},
    //        GreaterSpellPenetration :   { 'isSelected': false},
    //        HeightenSpell :   { 'isSelected': false},
            'Improved Bull Rush':   { 'isSelected': false},
    //        ImprovedCounterSpell:   { 'isSelected': false},
            'Improved Critical':   { 'isSelected': false},
            'Improved Disarm':   { 'isSelected': false},
    //        ImprovedFamiliar:   { 'isSelected': false},
            'Improved Feint':   { 'isSelected': false},
            'Improved Grapple':   { 'isSelected': false},
            'Improved Initiative':   { 'isSelected': false},
            'Improved Overrun':   { 'isSelected': false},
            'Improved Presice Shot':   { 'isSelected': false},
            'Improved Shield Bash':   { 'isSelected': false},
            'Improved Sunder':   { 'isSelected': false},
            'Improved Trip':   { 'isSelected': false},
    //        ImprovedTurning:   { 'isSelected': false},
            'Improved Two-Weapon Fighting':   { 'isSelected': false},
            'Improved Unarmed Strike':   { 'isSelected': false},
            'Investigator'      : { 'isSelected': false, 'Skills' : { 'Gather Information' : 2, 'Search' : 2 } },
            'Iron Will':   { 'isSelected': false},
            'Leadership':   { 'isSelected': false},
            'Lightning Reflexes':   { 'isSelected': false},
            'Magical Aptitude'  : { 'isSelected': false, 'Skills' : { 'Spellcraft' : 2, 'Use Magic Device' : 2 } },
            'Manyshot':   { 'isSelected': false},
            'Martial Weapon Proficiency':   { 'isSelected': false},
    //        MaximizeSpell:   { 'isSelected': false},
            'Mobility':   { 'isSelected': false},
            'Mounted Archery':   { 'isSelected': false},
            'Mounted Combat':   { 'isSelected': false},
    //        NaturalSpell:   { 'isSelected': false},
            'Negotiator'        : { 'isSelected': false, 'Skills' : { 'Diplomacy' : 2, 'Sense Motive' : 2 } },
            'Nimble Fingers'        : { 'isSelected': false, 'Skills' : { 'Disable Device' : 2, 'Open Lock' : 2 } },
            'Persuasive'        : { 'isSelected': false, 'Skills' : { 'Bluff' : 2, 'Intimidate' : 2 } },
            'Point Blank Shot':   { 'isSelected': false},
            'Power Attack':   { 'isSelected': false},
            'Precise Shot':   { 'isSelected': false},
            'Quick Draw':   { 'isSelected': false},
    //        QuickenSpell:   { 'isSelected': false},
            'Rapid Reload':   { 'isSelected': false},
            'Rapid Shot':   { 'isSelected': false},
            'Ride By Attack':   { 'isSelected': false},
            'Run':   { 'isSelected': false},
    //        ScribeScroll:   { 'isSelected': false},
            'Self Sufficient'       : { 'isSelected': false, 'Skills' : { 'Heal' : 2, 'Survival' : 2 } },
            'Shield Proficiency':   { 'isSelected': false},
            'Shot On The Run':   { 'isSelected': false},
    //        SilentSpell:   { 'isSelected': false},
            'Simple Weapon Proficiency':   { 'isSelected': false},
            'Skill Focus'       : { 'isSelected': false},
            'Snatch Arrows'     : { 'isSelected': false},
    //      SpellFocus      : { 'isSelected': false},
    //      SpellMastery        : { 'isSelected': false},
    //      SpellPenetration        : { 'isSelected': false},
            'Spirited Charge'       : { 'isSelected': false},
            'Spirng Attack'     : { 'isSelected': false},
            'Stealthy'      : { 'isSelected': false, 'Skills' : { 'Hide' : 2, 'Move Silently' : 2 } },
    //      StillSpell      : { 'isSelected': false},
            'Toughness'     : { 'isSelected': false},
            'Tower Shield Proficiency'      : { 'isSelected': false},
            'Track'     : { 'isSelected': false},
            'Trample'       : { 'isSelected': false},
            'TwoWeapon Defence'     : { 'isSelected': false},
            'TwoWeapon Fighting'        : { 'isSelected': false},
            'Weapon Finesse'        : { 'isSelected': false},
            'Weapon Focus'      : { 'isSelected': false},
            'Weapon Specialization'     : { 'isSelected': false},
            'Whirlwind Attack'      : { 'isSelected': false}
    //      WidenSpell      : { 'isSelected': false},
        };

        return that;
    };


    /*! Utility function which checks if the argument is an array.
     * @param obj The object to check
     * @return true if it is an Array.  */
    var isArray = function(obj) {
        return toString.call(obj) === "[object Array]";
    };

    /*! Utility function which adds all src items to dest if they are not already in there.
     * @param dest The Array to copy to
     * @param src The Array to copy from.  */
    var addIfNotAlreadyThere = function(dest, src) {
        if (!isArray(dest) || !isArray(src)) {
            return;
        }

        var notFound = true;
        // for each item in src list
        for(var itemSrc in src) {
            // compare with each item in dest list
            for(var itemDest in dest) {
                // if the item was found skip the rest
                if(itemSrc === itemDest) {
                    notFound = false;
                    break;
                }
            }

            if(notFound) {
                // couldn't find item
                // adding it
                dest[dest.length] = itemSrc;
            }

            notFound = true;
        }
    };

    /*! Utility function which does nested Array copy. Function and Object
     * references are substituted for null by default.
     * @param v The array to copy from
     * @return r The new copy of v */
    var deepArrayCopy = function(v) {
        // a new empty array
        var r = [];
        if(!isArray(v)) {
            return r;
        }

        for(var i=0; i<v.length; i++) {
            if(typeof v[i] === "function" ) {
                r[i] = null;
            }
            else if(isArray(v[i])) {
                r[i] = deepArrayCopy(v[i]);
            }
            else if(typeof (v[i]) === 'object') {
                r[i] = null;
            }
            else if(typeof (v[i]) !== 'object' &&
               typeof (v[i]) !== 'undefined' &&
               v && v[i] !== null) { 
                r[i] = v[i];
            }
        }
        return r;
    };


    // =============================================================
    // Public stuff
    
    /*! Maker Function: Creates the player character sheet.
     * @param parentChar Another character sheet to copy properties from or null/undefined for new
     * @return the character sheet object  */
    that.csCreateCharacter = function(parentChar) {
        // create new object
        var that = Object.create(characterUtilFunctions, startingCharacterProperties);

        // create skills object
        that.Skills = csCreateSkills();

        // create feats object
        that.Feats = csCreateFeats();

        // TODO: remove
        that.Inventory.push('Wrong Longsword');
        that.Inventory.push('Wooden Shield +1');
        that.Inventory.push('Bronze Necklace');
        that.Inventory.push('Bronze Ring');
        that.Inventory.push('Bronze Ring');
        that.Inventory.push('Metal Gloves');
        that.Inventory.push('Metal Helmet');
        that.Inventory.push('Hood');
        that.Inventory.push('Leather Belt');
        that.Inventory.push('Plate Armor');
        that.Inventory.push('Metal Pants');
        that.Inventory.push('Metal Boots');
        // TODO: end remove
     
        // if leveling up / copying values
        if (parentChar) {
            for(var itemP in parentChar) {
                if(parentChar.hasOwnProperty(itemP) && that.hasOwnProperty(itemP)) {
                    if(typeof parentChar[itemP] === "function" ) {
                        // do nothing
                    }
                    else if(isArray(parentChar[itemP])) {
                        that[itemP] = deepArrayCopy(parentChar[itemP]);
                    }
                    else if(typeof (parentChar[itemP]) === 'object') {
                        // do nothing 
                    }
                    else if(typeof (parentChar[itemP]) !== 'object' &&
                       typeof (parentChar[itemP]) !== 'undefined' &&
                       parentChar && parentChar[itemP] !== null) { 
                        that[itemP] = parentChar[itemP];
                    }
                }
            }
            that.remainingSkills = parentChar.remainingSkills;
            // copy skill ranks
            for(var skl in parentChar.Skills) {
                that.Skills[skl][0] = parentChar.Skills[skl][0];
                that.Skills[skl][1] = parentChar.Skills[skl][1];
            }
            // copy selected feats
            for(var ft in parentChar.Feats) {
                that.Feats[ft].isSelected = parentChar.Feats[ft].isSelected;
            }
        }

        /*! Returns an object containing available player actions.
         * @return An object with available actions.
         * TODO */
        that.actions = function() {
            return {};
        };

        return that;
    };

    // TODO: rethink about interface calls

    // Character Sheet interface
    that.SheetGroupNames = {
      'Information' : ['Name', 'Age', 'Gender', 'Race', 'Class', 'Languages'], 
      'CombatStats' : ['BAB', 'Melee', 'Ranged'],
      'Attributes'  : ['Str', 'Dex', 'Con', 'Int', 'Wis', 'Cha'],
      'Skills'      : ['Skills'],
      'Feats'       : ['Feats']
    };

    return that;
}();
