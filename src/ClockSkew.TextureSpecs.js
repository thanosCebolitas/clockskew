var basicCharStates = {
    "magicN" : [ 0, 1, 9 ],
    "magicW" : [ 0, 2, 9 ],
    "magicE" : [ 0, 3, 9 ],
    "magicS" : [ 0, 4, 9 ],

    "spearN" : [ 0, 5, 9 ],
    "spearW" : [ 0, 6, 9 ],
    "spearE" : [ 0, 7, 9 ],
    "spearS" : [ 0, 8, 9 ],

    "standN" : [ 0, 9, 1 ],
    "standW" : [ 0, 10, 1 ],
    "standE" : [ 0, 11, 1 ],
    "standS" : [ 0, 12, 1 ],

    "walkN" : [ 0, 9, 9 ],
    "walkW" : [ 0, 10, 9 ],
    "walkE" : [ 0, 11, 9 ],
    "walkS" : [ 0, 12, 9 ],

    "swordN" : [ 0, 13, 9 ],
    "swordW" : [ 0, 14, 9 ],
    "swordE" : [ 0, 15, 9 ],
    "swordS" : [ 0, 16, 9 ],

    "bowN" : [ 0, 17, 9 ],
    "bowW" : [ 0, 18, 9 ],
    "bowE" : [ 0, 19, 9 ],
    "bowS" : [ 0, 20, 9 ],
    "die" : [ 0, 21, 6 ],

    // non-existent
    "jumpN" : [ 0, 8, 1 ],
    "jumpW" : [ 0, 9, 1 ],
    "jumpE" : [ 0, 10, 1 ],
    "jumpS" : [ 0, 11, 1 ],

    "climbN" : [ 0, 1, 9 ],
    "climbW" : [ 0, 1, 9 ],
    "climbE" : [ 0, 1, 9 ],
    "climbS" : [ 0, 1, 9 ],

    "swimN" : [ 0, 0, 9 ],
    "swimW" : [ 0, 0, 9 ],
    "swimE" : [ 0, 0, 9 ],
    "swimS" : [ 0, 0, 9 ]
};

var csTextures = {
    // =======================================
    // Buildings
    'wall1': {
         file : "textures/terrain/dirt.png",
         rows : 1/6,
         cols : 1/6,
    },
    // =======================================
    // Objects 
    'barrel': {
         file : "textures/objects/barrel.png",
    },
    'bucket': {
         file : "textures/objects/bucket.png",
         rows : 1,
         cols : 2,
         anim : {
            "empty" : [ 0, 0, 1 ],
            "full" : [ 0, 1, 1 ]
         }
    },
    // =======================================
    // characters 
    'humanMale': {
         file : "textures/characters/BODY/body_male.png",
         rows : 21,
         cols : 13,
         animations : basicCharStates
    },
    'skeleton': {
         file : "textures/characters/BODY/skeleton.png",
         rows : 21,
         cols : 13,
         animations : basicCharStates
    },

    // =======================================
    // equipment/items 
    
    // ------------------------
    // weapons
    'dagger': {
         file : "textures/characters/WEAPON/dagger.png",
         rows : 21,
         cols : 13,
         animations : basicCharStates
    },
    // ------------------------
    // shields
    'shield': {
         file : "textures/characters/WEAPON/shield (cutout body).png",
         rows : 21,
         cols : 13,
         animations : basicCharStates
    },
    // ------------------------
    // belts 
    'belt': {
         file : "textures/characters/BELT/belt_leather_(chain).png",
         rows : 21,
         cols : 13,
         animations : basicCharStates
    },
    // ------------------------
    // armor parts - hands 
    'gloves': {
         file : "textures/characters/HANDS/gloves_metal_(plate).png",
         rows : 21,
         cols : 13,
         animations : basicCharStates
    },
    // ------------------------
    // armor parts - head 
    'helmet': {
         file : "textures/characters/HEAD/helmet_metal_(plate).png",
         rows : 21,
         cols : 13,
         animations : basicCharStates
    },
    'hood': {
         file : "textures/characters/HEAD/hood_(robe).png",
         rows : 21,
         cols : 13,
         animations : basicCharStates
    },
    // ------------------------
    // armor parts - torso 
    'plate': {
         file : "textures/characters/TORSO/torso_(plate).png",
         rows : 21,
         cols : 13,
         animations : basicCharStates
    },
    // ------------------------
    // armor parts - legs 
    'pants': {
         file : "textures/characters/LEGS/pants_metal_(plate).png",
         rows : 21,
         cols : 13,
         animations : basicCharStates
    },
    // ------------------------
    // armor parts - feet 
    'boots': {
         file : "textures/characters/FEET/boots_metal_(plate).png",
         rows : 21,
         cols : 13,
         animations : basicCharStates
    },

    // ------------------------
    // dummy temp texture to keep track of animation states
    'dummy': {
         file : "textures/objects/barrel.png",
         animations : basicCharStates 
    }
};

