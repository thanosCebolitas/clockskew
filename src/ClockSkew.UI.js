// ------------------------------------------------
// Utility
function isFunction(x) {
    return Object.prototype.toString.call(x) == '[object Function]';
}

// ------------------------------------------------
// Engine



// ------------------------------------------------
// GUI

var csGUICommandManager = function() {
    var that = {};

    return that;
};

var csComponents = function() {
    // class shared scope

    // ------------------------
    // private

    // ------------------------
    // vars
    var classScope = {};
    var allCsComponents = [];	
    var backgroundTexture = new Image();
    var backgroundPaperTexture = new Image();
    var backgroundInventoryTexture = new Image();
    var iconsTexture = new Image();
    var componentWithFocus = null;
    var componentDragged = null;
    var totalImages = 4;

    // ------------------------
    // functions
    var imageHasLoaded = function() {
        totalImages = totalImages - 1;
        repaintAllGui();
    };

    var createGUIMaterials = function(itemId, base, text, component, align) {
        if(typeof(itemId)==='undefined') itemId = 'buttonNormal';
        if(typeof(base)==='undefined') base = 'buttonNormal';
        if(typeof(text)==='undefined') text = '';
        if(align!=='left') align = 'center';

        if (UIItems.hasOwnProperty(base)) {
            // create a canvas object
            var canvas = document.createElement('canvas');
            var context = canvas.getContext('2d');
            var dim = UIItems[base];
            canvas.width = component.getSizeX();
            canvas.height = component.getSizeY();

            // canvas contents will be used for a texture
            var texture = new THREE.Texture(canvas);

            // manipulate image using background
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.rect(0, 0, canvas.width, canvas.height);

            if(base !== 'default') {
                var cnr = 10;
                context.drawImage(dim.tex, dim.x,           dim.y,      cnr,            cnr, 0, 0, cnr, cnr);
                context.drawImage(dim.tex, dim.x+cnr,       dim.y,      dim.w-cnr-cnr,  cnr, cnr, 0, canvas.width-cnr-cnr, cnr);
                context.drawImage(dim.tex, dim.x+dim.w-cnr, dim.y,      cnr,            cnr, canvas.width-cnr, 0, cnr, cnr);

                context.drawImage(dim.tex, dim.x,           dim.y+cnr,  cnr,            dim.h-cnr-cnr, 0, cnr, cnr, canvas.height-cnr-cnr);
                context.drawImage(dim.tex, dim.x+cnr,       dim.y+cnr,  dim.w-cnr-cnr,  dim.h-cnr-cnr, cnr, cnr, canvas.width-cnr-cnr, canvas.height-cnr-cnr);
                context.drawImage(dim.tex, dim.x+dim.w-cnr, dim.y+cnr,  cnr,            dim.h-cnr-cnr, canvas.width-cnr, cnr, cnr, canvas.height-cnr-cnr);

                context.drawImage(dim.tex, dim.x,           dim.y+dim.h-cnr,      cnr,            cnr, 0, canvas.height-cnr, cnr, cnr);
                context.drawImage(dim.tex, dim.x+cnr,       dim.y+dim.h-cnr,      dim.w-cnr-cnr,  cnr, cnr, canvas.height-cnr, canvas.width-cnr-cnr, cnr);
                context.drawImage(dim.tex, dim.x+dim.w-cnr, dim.y+dim.h-cnr,      cnr,            cnr, canvas.width-cnr, canvas.height-cnr, cnr, cnr);
            }

            // if we have any text to draw
            if (text !== '') {
                var textSize = 18;

                context.font = 'Bold '+ textSize +'px Verdana';
//                context.font = 'Bold '+ textSize +'px Helvetica';
//                context.font = 'Bold '+ textSize +'px Helvetica';
                context.textAlign = align;
                context.textBaseline = "middle";
                context.fillStyle = 'white';

                context.strokeStyle = 'black';
                context.lineWidth=3.0;

                if(align === 'center') {
                    context.strokeText(text, component.getSizeX()/2, component.getSizeY()/2);
                    context.fillText(text, component.getSizeX()/2, component.getSizeY()/2);
                }
                else {
                    var lmarg = 5;
                    context.strokeText(text, lmarg, component.getSizeY()/2);
                    context.fillText(text, lmarg, component.getSizeY()/2);
                }
            }

            // schedule for update
            texture.needsUpdate = true;

            // create material
            UIMaterials[itemId] = new THREE.SpriteMaterial( { map: texture, useScreenCoordinates: true } );
        }

        return true;
    };

    var imageFailedToLoad = function() {
        // TODO:
    };

    var isItemInArray = function(it, ar) {
        for(var i=0; i<ar.length; i++) {
            if(it === ar[i]) {
                return true;
            }
        }

        return false;
    };

    // ------------------------
    // public 

    //-------------------------
    // vars

    // ui tile sheet items
    var UIItems = {
        // group button
        'groupButtonNormal'	:	{x:14, y:11, w:28, h:28, tex:backgroundTexture},
        'groupButtonOver'	:	{x:49, y:11, w:28, h:28, tex:backgroundTexture},
        'groupButtonDisabled'	:	{x:84, y:11, w:28, h:28, tex:backgroundTexture},
        'groupButtonNormalChecked'	:	{x:14, y:47, w:28, h:28, tex:backgroundTexture},
        'groupButtonOverChecked'	:	{x:49, y:47, w:28, h:28, tex:backgroundTexture},
        'groupButtonDisabledChecked'	:	{x:84, y:47, w:28, h:28, tex:backgroundTexture},

        // check box
        'checkBoxNormal'	:	{x:148, y:11, w:28, h:28, tex:backgroundTexture},
        'checkBoxOver'	:	{x:193, y:11, w:28, h:28, tex:backgroundTexture},
        'checkBoxDisabled'	:	{x:238, y:11, w:28, h:28, tex:backgroundTexture},
        'checkBoxNormalChecked'	:	{x:148, y:55, w:28, h:28, tex:backgroundTexture},
        'checkBoxOverChecked'	:	{x:193, y:55, w:28, h:28, tex:backgroundTexture},
        'checkBoxDisabledChecked'	:	{x:238, y:55, w:28, h:28, tex:backgroundTexture},

        // buttons
        'buttonNormal'	:	{x:11, y:120, w:290, h:67, tex:backgroundTexture},
        'buttonOver'	:	{x:11, y:276, w:290, h:67, tex:backgroundTexture},
        'buttonClicked'	:	{x:11, y:198, w:290, h:67, tex:backgroundTexture},
        'buttonDisabled'	:	{x:11, y:354, w:290, h:67, tex:backgroundTexture},

        // +/- horizontal buttons
        'buttonHMinusNormal'	:	{x:324, y:16, w:32, h:32, tex:backgroundTexture},
        'buttonHMinusClicked'	:	{x:324, y:68, w:32, h:32, tex:backgroundTexture},
        'buttonHMinusOver'	:	{x:324, y:120, w:32, h:32, tex:backgroundTexture},
        'buttonHMinusDisabled'	:	{x:324, y:171, w:32, h:32, tex:backgroundTexture},
        'buttonHPlusNormal'	:	{x:363, y:16, w:32, h:32, tex:backgroundTexture},
        'buttonHPlusClicked'	:	{x:363, y:68, w:32, h:32, tex:backgroundTexture},
        'buttonHPlusOver'	:	{x:363, y:120, w:32, h:32, tex:backgroundTexture},
        'buttonHPlusDisabled'	:	{x:363, y:171, w:32, h:32, tex:backgroundTexture},
        'buttonDownNormal'	:	{x:656, y:16, w:32, h:32, tex:backgroundTexture},
        'buttonDownClicked'	:	{x:656, y:68, w:32, h:32, tex:backgroundTexture},
        'buttonDownOver'	:	{x:656, y:120, w:32, h:32, tex:backgroundTexture},
        'buttonDownDisabled'	:	{x:656, y:171, w:32, h:32, tex:backgroundTexture},
        'buttonUpNormal'	:	{x:694, y:16, w:32, h:32, tex:backgroundTexture},
        'buttonUpClicked'	:	{x:694, y:68, w:32, h:32, tex:backgroundTexture},
        'buttonUpOver'	:	{x:694, y:120, w:32, h:32, tex:backgroundTexture},
        'buttonUpDisabled'	:	{x:694, y:171, w:32, h:32, tex:backgroundTexture},

        // icons
        'hlmt1'	:	{x:0, y:0, w:32, h:32, tex:iconsTexture},
        'hlmt2'	:	{x:32, y:0, w:32, h:32, tex:iconsTexture},
        'hlmt3'	:	{x:64, y:0, w:32, h:32, tex:iconsTexture},

        'boot1'	:	{x:0, y:32, w:32, h:32, tex:iconsTexture},
        'boot2'	:	{x:32, y:32, w:32, h:32, tex:iconsTexture},
        'boot3'	:	{x:64, y:32, w:32, h:32, tex:iconsTexture},

        'botl1'	:	{x:0, y:64, w:32, h:32, tex:iconsTexture},
        'botl2'	:	{x:32, y:64, w:32, h:32, tex:iconsTexture},
        'botl3'	:	{x:64, y:64, w:32, h:32, tex:iconsTexture},

        'nklc1'	:	{x:0, y:96, w:32, h:32, tex:iconsTexture},
        'nklc2'	:	{x:32, y:96, w:32, h:32, tex:iconsTexture},
        'nklc3'	:	{x:64, y:96, w:32, h:32, tex:iconsTexture},

        'swrd1'	:	{x:0, y:128, w:32, h:32, tex:iconsTexture},
        'swrd2'	:	{x:32, y:128, w:32, h:32, tex:iconsTexture},
        'swrd3'	:	{x:64, y:128, w:32, h:32, tex:iconsTexture},

        'axe1'	:	{x:0, y:160, w:32, h:32, tex:iconsTexture},
        'axe2'	:	{x:32, y:160, w:32, h:32, tex:iconsTexture},
        'axe3'	:	{x:64, y:160, w:32, h:32, tex:iconsTexture},

        'club1'	:	{x:0, y:192, w:32, h:32, tex:iconsTexture},
        'club2'	:	{x:32, y:192, w:32, h:32, tex:iconsTexture},
        'club3'	:	{x:64, y:192, w:32, h:32, tex:iconsTexture},

        'spear1'	:	{x:0, y:224, w:32, h:32, tex:iconsTexture},
        'spear2'	:	{x:32, y:224, w:32, h:32, tex:iconsTexture},
        'spear3'	:	{x:64, y:224, w:32, h:32, tex:iconsTexture},

        'staff1'	:	{x:0, y:256, w:32, h:32, tex:iconsTexture},
        'staff2'	:	{x:32, y:256, w:32, h:32, tex:iconsTexture},
        'staff3'	:	{x:64, y:256, w:32, h:32, tex:iconsTexture},

        'shld1'	:	{x:0, y:288, w:32, h:32, tex:iconsTexture},
        'shld2'	:	{x:32, y:288, w:32, h:32, tex:iconsTexture},
        'shld3'	:	{x:64, y:288, w:32, h:32, tex:iconsTexture},

        // random icons
        'deleteBtnNormal'	:	{x:620, y:400, w:32, h:32, tex:backgroundTexture},
        'deleteBtnOver'	:	{x:659, y:400, w:32, h:32, tex:backgroundTexture},
        'deleteBtnClicked'	:	{x:698, y:400, w:32, h:32, tex:backgroundTexture},

        'duplicateBtnNormal'	:	{x:620, y:452, w:32, h:32, tex:backgroundTexture},
        'duplicateBtnOver'	:	{x:659, y:452, w:32, h:32, tex:backgroundTexture},
        'duplicateBtnClicked'	:	{x:698, y:452, w:32, h:32, tex:backgroundTexture},

        'newBtnNormal'	:	{x:620, y:504, w:32, h:32, tex:backgroundTexture},
        'newBtnOver'	:	{x:659, y:504, w:32, h:32, tex:backgroundTexture},
        'newBtnClicked'	:	{x:698, y:504, w:32, h:32, tex:backgroundTexture},

        // other textures
        'paper'	:	{x:0, y:0, w:512, h:512, tex:backgroundPaperTexture},
        'inventorybg'	:	{x:0, y:0, w:280, h:388, tex:backgroundInventoryTexture},
        'default' :	{x:0, y:0, w:64, h:64, tex:backgroundPaperTexture},
        'tile' :	{x:40, y:489, w:42, h:42, tex:backgroundTexture},
        'label' :	{x:400, y:500, w:164, h:37, tex:backgroundTexture},
        'label2' :	{x:400, y:450, w:164, h:37, tex:backgroundTexture},
        'label3' :	{x:400, y:396, w:164, h:37, tex:backgroundTexture},
        'invItem' :	{x:39, y:541, w:45, h:45, tex:backgroundTexture},
        'invEqItem' :	{x:109, y:541, w:45, h:45, tex:backgroundTexture},
        '' :	{x:0, y:0, w:64, h:64, tex:backgroundPaperTexture}
    };
    // make public
    classScope.UIItems = UIItems;
    var UIMaterials = {};
    classScope.UIMaterials = UIMaterials;

    // ------------------------
    // functions
    var addChild = function(chld) {
        if(chld && !isItemInArray(chld, allCsComponents)) {
            allCsComponents.push(chld);
            chld.paint();
        }
    };
    classScope.addChild = addChild;

    var handleMouseMoveEvent = function(e) {
        var comp = null;

        for(var i=0; i<allCsComponents.length; i++) {
            if (allCsComponents[i].isVisible()) {
                if( ! allCsComponents[i].isInterestedInEvent(e) ) {
                    continue;
                }

                comp = allCsComponents[i].handleMouseMoveEvent(e);
                if(comp && comp !== componentWithFocus ) {
                    if (componentWithFocus && componentDragged !== componentWithFocus) {
                        componentWithFocus.handleMouseExitEvent(e);
                    }
                    componentWithFocus = comp;
                    if (!componentDragged) {
                        return componentWithFocus;
                    }
                }
                else if(comp) {
                    return comp;
                }
            }
        }

        if (componentWithFocus) {
            componentWithFocus.handleMouseExitEvent(e);
            componentWithFocus = null;
        }

        return componentDragged ? componentDragged : null;
    };
    classScope.handleMouseMoveEvent = handleMouseMoveEvent;

    var handleMouseDownEvent = function(e) {
        var comp = null;
        for(var i=0; i<allCsComponents.length; i++) {
            if (allCsComponents[i].isVisible()) {
                comp = allCsComponents[i].handleMouseDownEvent(e);
                if(comp) {
                    componentWithFocus = comp;
                    return componentWithFocus;
                }
            }
        }
        return null;
    };
    classScope.handleMouseDownEvent = handleMouseDownEvent;

    var handleMouseUpEvent = function(e) {
        var comp = null;
        for(var i=0; i<allCsComponents.length; i++) {
            if (allCsComponents[i].isVisible()) {
                comp = allCsComponents[i].handleMouseUpEvent(e);
                if(comp) {
                    componentWithFocus = comp;
                    return componentWithFocus;
                }
            }
        }
        return null;
    };
    classScope.handleMouseUpEvent = handleMouseUpEvent;

    var setDraggingComponent = function(comp) {
        componentDragged = comp;
    };
    classScope.setDraggingComponent = setDraggingComponent;

    var getDraggingComponent = function() {
       return  componentDragged;
    };
    classScope.getDraggingComponent = getDraggingComponent;

    var repaintAllGui = function() {
        var comp = null;
        for(var i=0; i<allCsComponents.length; i++) {
            comp = allCsComponents[i].paint();
        }
        return null;
    };
    classScope.repaintAllGui = repaintAllGui;

    var getComponentFromXY = function(x, y, origComp, cond) {
        for(var i=0; i<allCsComponents.length; i++) {
            if (allCsComponents[i].isVisible()) {
                var comp = allCsComponents[i].getComponentFromXY(x, y, origComp, cond);
                if ( comp && comp !== origComp ) {
                    return comp;
                }
            }
        }
        return null;
    };
    classScope.getComponentFromXY = getComponentFromXY;

    // ------------------------
    // basic component
    var createComponent = function(scene, base, text, sizeX, sizeY) {
        // ------------------------
        // private variables
        var that = {};
        that.onMouseDown = null;
        that.onMouseUp = null;

        var children = [];
        var threeObject = null;
        var visible = false;
        var paintingIsComplete = false;
        var snapComponent = null;
        var snapperComponent = null;

        var posX = 0;
        var posY = 0;
        var posZ = 0;
        // set default parameter
        if(typeof(base)==='undefined') base = 'default';
        if(typeof(text)==='undefined') text = '';
        if(typeof(sizeX)==='undefined') sizeX = UIItems[base].w;
        if(typeof(sizeY)==='undefined') sizeY = UIItems[base].h;

        that.align = 'center';

        // ------------------------
        // private methods

        // ------------------------
        // public methods
        var getPosition = function() {
            if (threeObject) {
                return threeObject.position;
            }
            return null;
        };
        that.getPosition = getPosition;

        var getThreeObject = function() {
            return threeObject;
        };
        that.getThreeObject = getThreeObject;

        var paint = function(force) {
            // set default parameter
            if(typeof(force)==='undefined') force = false;

            if (true) {
                var itemId = that.getUIItemId();
                var baseName = that.getUIItemBaseName();
                var textToShow = that.getText();

                // create sprite object
                if( threeObject && (itemId in UIMaterials) ) {
                    threeObject.material = UIMaterials[itemId];
                }
                else if ( (itemId in UIMaterials) || createGUIMaterials(itemId, baseName, textToShow, that, that.align) ) {
                    if(threeObject === null) {
                        threeObject = new THREE.Sprite( UIMaterials[itemId] );
                    }
                    else {
                        threeObject.material = UIMaterials[itemId];
                    }

                    // set size
                    if(sizeX && sizeY) {
                        that.setSize(sizeX, sizeY);
                    }
                    else {
                        that.setSize(UIItems[baseName].w, UIItems[baseName].h);
                    }
                }
                else {
                    return false;
                }

                paintingIsComplete = true;

                for(var i=0; i<children.length; i++) {
                    if(children[i]) {
                        children[i].paint();
                    }
                }
            }

            return true;
        };
        that.paint = paint;

        var addChild = function(chld) {
            // add child		
            if(chld && !isItemInArray(chld, children)) {
                children.push(chld);
                chld.paint(true);				
            }
        };
        that.addChild = addChild;

        var removeAllChildren = function(xcl) {
            for(var ch=0; ch<children.length; ch++) {
                if(!isItemInArray(children[ch], xcl)) {
                    children[ch].removeFromScene();
                }
            }
            children = xcl;
            that.paint();
        };
        that.removeAllChildren = removeAllChildren;

        // grab resources
        var setBackground = function() {
            that.paint(true);
        };
        that.setBackground = setBackground;

        var isInterestedInEvent = function(e) {
           if( threeObject && isVisible() && that.isWithinBounds(e.clientX - three.CANVAS_OFFSET_LEFT, three.CANVAS_HEIGHT - e.clientY + three.CANVAS_OFFSET_TOP) ) {
               return true;
           }
           return false;
        };
        that.isInterestedInEvent = isInterestedInEvent;

        var isWithinBounds = function(x, y) {
            if ( (x + threeObject.scale.x * 0.5 >= threeObject.position.x) &&
                 (x + threeObject.scale.x * 0.5 <= threeObject.position.x + threeObject.scale.x) &&
                 (y + threeObject.scale.y * 0.5 >= threeObject.position.y) &&
                 (y + threeObject.scale.y * 0.5 <= threeObject.position.y + threeObject.scale.y) ) {
                     return true;
            }
            else {
                return false;
            }
        };
        that.isWithinBounds = isWithinBounds;

        var getComponentFromXY = function(x, y, origComp, cond) {
            for (var i=0; i<children.length; i++) {
                var comp = null;
                if ( children[i] && (comp = children[i].getComponentFromXY(x, y, origComp, cond)) && comp !== origComp ) {
                    return comp;
                }
            }

            return ( isWithinBounds(x, y) && origComp !== that &&
                    ( ( that.hasOwnProperty(cond) && typeof(that[cond] === 'function') && that[cond](origComp) ) ||
                    !that.hasOwnProperty(cond) ) ) ? that : null;
        };
        that.getComponentFromXY = getComponentFromXY;

        var setSnapper = function(snapper) {
            snapperComponent = snapper;
            return that;
        };
        that.setSnapper = setSnapper;

        var getSnapper = function() {
            return snapperComponent;
        };
        that.getSnapper = getSnapper;

        var releaseSnapped = function() {
            var comp = snapComponent;
            snapComponent = null;
            return comp;
        };
        that.releaseSnapped = releaseSnapped;

        var snapCondition = function(other) {
            return false;
        };
        that.snapCondition = snapCondition;

        var canSnapThat = function(other) {
            return ( snapComponent === null && that.snapCondition(other) );
        };
        that.canSnapThat = canSnapThat;

        var snapThat = function(other) {
            if ( that.canSnapThat(other) ) {
                // set snapped
                snapComponent = other;
                // change snapper
                var otherSnapper = other.getSnapper();
                if (otherSnapper) {
                    otherSnapper.releaseSnapped();
                }
                other.setSnapper(that);

                // position
                var x = that.getPositionX();
                var y = that.getPositionY();
                other.setPosition(x, y, other.getPositionZ());
            }
            return that;
        };
        that.snapThat = snapThat;

        var handleMouseMoveEvent = function(e) {
            var consumed = null;

            //TODO: opt - if this is not interested neither should its children 

            for ( var i=0; i<children.length; i++ ) {
                if ( children[i] && (consumed = children[i].handleMouseMoveEvent(e)) ) {
                    if (!componentDragged) {
                        return consumed;
                    }
                }
            }
            return componentDragged ? componentDragged : null;
        };
        that.handleMouseMoveEvent = handleMouseMoveEvent;

        var handleMouseExitEvent = function(e) {
            var consumed = null;
            for(var i=0; i<children.length; i++) {
                if(children[i] && (consumed = children[i].handleMouseExitEvent(e)) ) {
                    return consumed;
                }
            }
            return null;
        };
        that.handleMouseExitEvent = handleMouseExitEvent;

        var handleMouseDownEvent = function(e) {
            var consumed = null;
            for(var i=0; i<children.length; i++) {
                if(children[i] && (consumed = children[i].handleMouseDownEvent(e)) ) {
                    return consumed;
                }
            }
            if (isFunction(that.onMouseDown)) {
                that.onMouseDown();
            }			
            return null;
        };
        that.handleMouseDownEvent = handleMouseDownEvent;

        var handleMouseUpEvent = function(e) {
            var consumed = null;

            var dcomp = csComponents.getDraggingComponent();
            if ( dcomp && ( consumed = dcomp.handleMouseExitEvent(e) ) ) {
                return consumed;
            }

            for(var i=0; i<children.length; i++) {
                if(children[i] && (consumed = children[i].handleMouseUpEvent(e)) ) {
                    return consumed;
                }
            }
            return null;
        };
        that.handleMouseUpEvent = handleMouseUpEvent;

        that.setPosition = function(x, y, z) {
            //TODO: positioning should move children too
            if (x && y && z) {
                posX = x;
                posY = y;
                posZ = z;
                if (threeObject) {
                    threeObject.position.set(posX, posY, posZ);
                }
            } 
            else if (threeObject) {
                threeObject.position.set(posX, posY, posZ);
            }
        };

        that.getPositionX = function() {
            return posX;
        };

        that.getPositionY = function() {
            return posY;
        };

        that.getPositionZ = function() {
            return posZ;
        };

        that.setSize = function(x, y, z) {
            if (x && y) {
                sizeX = x;
                sizeY = y;
                if(threeObject) {	
                    threeObject.scale.set(x, y, 1.0);
                }
            }
            else if(threeObject) {	
                threeObject.scale.set(sizeX, sizeY, 1.0);
            }
        };

        that.getSizeX = function() {
            return sizeX;
        };

        that.getSizeY = function() {
            return sizeY;
        };

        var getUIItemId = function() {
            return that.getUIItemBaseName()+that.getText();
        };
        that.getUIItemId = getUIItemId;

        var getUIItemBaseName = function() {
            return base;
        };
        that.getUIItemBaseName = getUIItemBaseName;

        var getText = function() {
            return text;
        };
        that.getText = getText;

        var handleKeyboardEvent = function(e) {
            for(var i=0; i<children.length; i++) {
                children[i].handleKeyboardEvent();
            }
        };
        that.handleKeyboardEvent = handleKeyboardEvent;

        var render = function() {
            // notify children
            for(var i=0; i<children.length; i++) {
                children[i].render();
            }
        };
        that.render = render;

        var addToScene = function() {
            if (threeObject && !visible) {
                scene.add(threeObject);
                for(var i=0; i<children.length; i++) {
                    children[i].addToScene(scene);
                }
                visible = true;
            }
        };
        that.addToScene = addToScene;

        var removeFromScene = function() {
            if (threeObject && visible) {
                scene.remove(threeObject);
                for(var i=0; i<children.length; i++) {
                    children[i].removeFromScene();
                }
                visible = false;				
            }
        };
        that.removeFromScene = removeFromScene;

        var isVisible = function() {
            return visible;
        };
        that.isVisible = isVisible;

        return that;
    };
    // make public
    classScope.createComponent = createComponent;

    // ------------------------
    // button subclass
    var createButtonComponent = function(scene, text, w, h) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';

        var that = createComponent(scene, '', text, w, h);
        var isMouseOver = false;
        var isMouseDown = false;
        var isDisabled = false;

        // ------------------------
        // private methods

        // ------------------------
        // public methods
        var getIsMouseDown = function() {
            return isMouseDown;
        };
        that.getIsMouseDown = getIsMouseDown;

        var getIsMouseOver = function() {
            return isMouseOver;
        };
        that.getIsMouseOver = getIsMouseOver;

        var setDisabled = function(v) {
            isDisabled = v;
        };
        that.setDisabled = setDisabled;

        var getIsDisabled = function() {
            return isDisabled;
        };
        that.getIsDisabled = getIsDisabled;

        var superHandleMouseMoveEvent = that.handleMouseMoveEvent;
        var handleMouseMoveEvent = function(e) {
            var comp = superHandleMouseMoveEvent(e);
            if(!comp && that.isInterestedInEvent(e) && !isDisabled) {
                isMouseOver = true;
                that.paint();
                return that;
            }
            else {
                return comp;
            }
        };
        that.handleMouseMoveEvent = handleMouseMoveEvent;

        var superHandleMouseExitEvent = that.handleMouseExitEvent;
        var handleMouseExitEvent = function(e) {
            var comp = superHandleMouseExitEvent(e);
            if(!comp) {
                isMouseOver = false;
                isMouseDown = false;
                that.paint();
                return that;
            }
            else {
                return comp;
            }
        };
        that.handleMouseExitEvent = handleMouseExitEvent;

        var superHandleMouseDownEvent = that.handleMouseDownEvent;
        var handleMouseDownEvent = function(e) {
            var comp = superHandleMouseDownEvent(e);
            if(!comp && that.isInterestedInEvent(e) && !isDisabled) {
                isMouseDown = true;
                that.paint();
                return that;
            }
            else {
                return comp;
            }
        };
        that.handleMouseDownEvent = handleMouseDownEvent;		

        var superHandleMouseUpEvent = that.handleMouseUpEvent;
        var handleMouseUpEvent = function(e) {
            var comp = superHandleMouseUpEvent(e);
            if(!comp && that.isInterestedInEvent(e) && !isDisabled) {
                if (isMouseDown && isFunction(that.onMouseUp)) {
                    that.onMouseUp();
                }
                isMouseDown = false;
                that.paint();
                return that;
            }
            else {
                return comp;
            }
        };
        that.handleMouseUpEvent = handleMouseUpEvent;

        that.getUIItemBaseName = function() {
            if(isMouseDown) { return 'buttonClicked'; }
            else if (isMouseOver) { return 'buttonOver'; }
            else if (isDisabled) { return 'buttonDisabled';	}
            else { return 'buttonNormal'; }
        };

        return that;
    };
    // make public
    classScope.createButtonComponent = createButtonComponent;

    
    // ------------------------
    // draggable content subclass
    var createDraggableComponent = function(scene, bg, text, w, h, releaseCallback) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';
        if(typeof(bg)==='undefined') bg = '';

        var startDragX = 0;
        var startDragY = 0;
        var baseName = 'swrd1';

        var that = createButtonComponent(scene, text, w, h);
        var hasStartedDragging = false;

        var onStartDragging = function(e) {
            hasStartedDragging = true;
            startDragX = that.getPositionX();
            startDragY = that.getPositionY();
            setDraggingComponent(that);
        };
        that.onStartDragging = onStartDragging;

        var onStopDragging = function(e) {
            hasStartedDragging = false;
            setDraggingComponent(null);
            if (typeof releaseCallback === 'function') {
                releaseCallback(that);
            }
        };
        that.onStopDragging = onStopDragging;

        var setPositionBeforeDragStart = function() {
            that.setPosition(startDragX, startDragY, that.getPositionZ());
        };
        that.setPositionBeforeDragStart = setPositionBeforeDragStart;

        var isBeingDragged = function() {
            return hasStartedDragging;
        };
        that.isBeingDragged = isBeingDragged;

        var isInterestedInEvent = function(e) {
            var threeObject = that.getThreeObject();
            if ( threeObject && that.isVisible() && ( (csComponents.getDraggingComponent() === that)  || 
               ( that.isWithinBounds(e.clientX - three.CANVAS_OFFSET_LEFT, three.CANVAS_HEIGHT - e.clientY + three.CANVAS_OFFSET_TOP) ) ) ) {
               return true;
           }
           return false;
        };
        that.isInterestedInEvent = isInterestedInEvent;

        var superHandleMouseMoveEvent = that.handleMouseMoveEvent;
        var handleMouseMoveEvent = function(e) {
            var comp = superHandleMouseMoveEvent(e);
            if(comp === that && that.isInterestedInEvent(e) && !that.getIsDisabled()) {
                if(that.getIsMouseDown() && !hasStartedDragging) {
                    that.onStartDragging(e);
                }
                if(hasStartedDragging) {
                    that.setPosition(e.clientX - three.CANVAS_OFFSET_LEFT, three.CANVAS_HEIGHT - e.clientY + three.CANVAS_OFFSET_TOP, that.getPositionZ());
                }
                that.paint();
                return that;
            }
            else {
                return comp;
            }
        };
        that.handleMouseMoveEvent = handleMouseMoveEvent;

        var superHandleMouseExitEvent = that.handleMouseExitEvent;
        var handleMouseExitEvent = function(e) {
            var comp = superHandleMouseExitEvent(e);
            if(comp === that) {
                if(hasStartedDragging) {
                    hasStartedDragging = false;
                    that.onStopDragging(e);
                }
                that.paint();
                return that;
            }
            else {
                return comp;
            }
        };
        that.handleMouseExitEvent = handleMouseExitEvent;

        var superHandleMouseDownEvent = that.handleMouseDownEvent;
        var handleMouseDownEvent = function(e) {
            var comp = superHandleMouseDownEvent(e);
            if(comp === that && that.isInterestedInEvent(e) && !that.getIsDisabled()) {
                that.paint();
                return that;
            }
            else {
                return comp;
            }
        };
        that.handleMouseDownEvent = handleMouseDownEvent;		

        var superHandleMouseUpEvent = that.handleMouseUpEvent;
        var handleMouseUpEvent = function(e) {
            var comp = superHandleMouseUpEvent(e);
            if(comp === that && that.isInterestedInEvent(e) && !that.getIsDisabled()) {
                if (hasStartedDragging) {
                    that.onStopDragging(e);
                }
                that.paint();
                return that;
            }
            else {
                return comp;
            }
        };
        that.handleMouseUpEvent = handleMouseUpEvent;

        var setUIItemBaseName = function(newBaseName) {
            baseName = newBaseName;
        };
        that.setUIItemBaseName = setUIItemBaseName;

        that.getUIItemBaseName = function() {
            return baseName;
        };

        return that;
    };
    classScope.createDraggableComponent = createDraggableComponent;

    // ------------------------
    // checkbox subclass
    var createCheckBoxComponent = function(scene, text, w, h, beforeCallback, afterCallback, isChecked) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';
        if(typeof(isChecked)==='undefined') isChecked = false;

        var that = createButtonComponent(scene, '', w, h);

        var lbl = null;
        if(typeof(text)==='undefined') {
            text = '';
        }
        else {
            lbl = csComponents.createTextEditComponent(scene, 350, h);
            lbl.setText(text);
            that.addChild(lbl);
        }

        // ------------------------
        // private methods

        // ------------------------
        // public methods

        var superSetPosition = that.setPosition;
        that.setPosition = function(x, y, z) {
            superSetPosition(x, y, z);
            if(lbl !== null) {
                lbl.setPosition(x-lbl.getSizeX()*0.5, y, z);
            }
        };

        var getIsChecked = function() {
            return isChecked;
        };
        that.getIsChecked = getIsChecked;

        var setState = function(state) {
            isChecked = state;
            that.paint();

            return that;
        };
        that.setState = setState;

        var toggleState = function(force) {
            if(typeof beforeCallback === 'function') {
               if(beforeCallback(isChecked, ! isChecked) === false) {
                   return null;
               }
            }

            isChecked = ( typeof force === undefined ? false : !!force ) || ( ! isChecked);

            if(typeof afterCallback === 'function') {
               if(afterCallback(isChecked, ! isChecked, text, that) === false) {
                   return null;
               }
            }

            that.paint();

            return that;
        };
        that.toggleState = toggleState;

        var superHandleMouseUpEvent = that.handleMouseUpEvent;
        var handleMouseUpEvent = function(e) {
            var comp = superHandleMouseUpEvent(e);
            if(comp === that) {
                toggleState();
                return that;
            }
            else {
                return comp;
            }
        };
        that.handleMouseUpEvent = handleMouseUpEvent;

        that.getUIItemBaseName = function() {
            if(isChecked) {
                if(that.getIsMouseDown()) { return 'checkBoxNormalChecked'; }
                else if (that.getIsMouseOver()) { return 'checkBoxOverChecked'; }
                else if (that.getIsDisabled()) { return 'checkBoxDisabledChecked'; }
                else { return 'checkBoxNormalChecked'; }
            }
            else {
                if(that.getIsMouseDown()) { return 'checkBoxNormal'; }
                else if (that.getIsMouseOver()) { return 'checkBoxOver'; }
                else if (that.getIsDisabled()) { return 'checkBoxDisabled'; }
                else { return 'checkBoxNormal'; }
            }
        };

        return that;
    };
    // make public
    classScope.createCheckBoxComponent = createCheckBoxComponent;

    // ------------------------
    // checkbox subclass
    var createGroupBoxComponent = function(scene, text, w, h) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';

        var that = createCheckBoxComponent(scene, text, w, h);

        // ------------------------
        // private methods

        // ------------------------
        // public methods

        that.getUIItemBaseName = function() {
            if(that.getIsChecked()) {
                if(that.getIsMouseDown()) { return 'groupButtonNormalChecked'; }
                else if (that.getIsMouseOver()) { return 'groupButtonOverChecked'; }
                else if (that.getIsDisabled()) { return 'groupButtonDisabledChecked'; }
                else { return 'groupButtonNormalChecked'; }
            }
            else {
                if(that.getIsMouseDown()) { return 'groupButtonNormal'; }
                else if (that.getIsMouseOver()) { return 'groupButtonOver'; }
                else if (that.getIsDisabled()) { return 'groupButtonDisabled'; }
                else { return 'groupButtonNormal'; }
            }
        };

        return that;
    };
    // make public
    classScope.createGroupBoxComponent = createGroupBoxComponent;

    // ------------------------
    // delete icon + button subclass
    var createDeleteButtonComponent = function(scene, text, w, h) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';

        var that = createButtonComponent(scene, text, w, h);

        // ------------------------
        // private methods

        // ------------------------
        // public methods

        that.getUIItemBaseName = function() {
            if(that.getIsMouseDown()) { return 'deleteBtnClicked'; }
            else if (that.getIsMouseOver()) { return 'deleteBtnOver'; }
            else if (that.getIsDisabled()) { return 'deleteBtnNormal';	}
            else { return 'deleteBtnNormal'; }
        };

        return that;
    };
    // make public
    classScope.createDeleteButtonComponent = createDeleteButtonComponent;

    // ------------------------
    // duplicate icon + button subclass
    var createDuplicateButtonComponent = function(scene, text, w, h) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';

        var that = createButtonComponent(scene, text, w, h);

        // ------------------------
        // private methods

        // ------------------------
        // public methods

        that.getUIItemBaseName = function() {
            if(that.getIsMouseDown()) { return 'duplicateBtnClicked'; }
            else if (that.getIsMouseOver()) { return 'duplicateBtnOver'; }
            else if (that.getIsDisabled()) { return 'duplicateBtnNormal';	}
            else { return 'duplicateBtnNormal'; }
        };

        return that;
    };
    // make public
    classScope.createDuplicateButtonComponent = createDuplicateButtonComponent;

    // ------------------------
    // new icon + button subclass
    var createNewButtonComponent = function(scene, text, w, h) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';

        var that = createButtonComponent(scene, text, w, h);

        // ------------------------
        // private methods

        // ------------------------
        // public methods

        that.getUIItemBaseName = function() {
            if(that.getIsMouseDown()) { return 'newBtnClicked'; }
            else if (that.getIsMouseOver()) { return 'newBtnOver'; }
            else if (that.getIsDisabled()) { return 'newBtnNormal';	}
            else { return 'newBtnNormal'; }
        };

        return that;
    };
    // make public
    classScope.createNewButtonComponent = createNewButtonComponent;

    // ------------------------
    // horizontal + button subclass
    var createHPButtonComponent = function(scene, text, w, h) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';

        var that = createButtonComponent(scene, text, w, h);

        // ------------------------
        // private methods

        // ------------------------
        // public methods

        that.getUIItemBaseName = function() {
            if(that.getIsMouseDown()) { return 'buttonHPlusClicked'; }
            else if (that.getIsMouseOver()) { return 'buttonHPlusOver'; }
            else if (that.getIsDisabled()) { return 'buttonHPlusDisabled';	}
            else { return 'buttonHPlusNormal'; }
        };

        return that;
    };
    // make public
    classScope.createHPButtonComponent = createHPButtonComponent;

    // ------------------------
    // horizontal - button subclass
    var createHMButtonComponent = function(scene, text, w, h) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';

        var that = createButtonComponent(scene, text, w, h);

        // ------------------------
        // private methods

        // ------------------------
        // public methods

        that.getUIItemBaseName = function() {
            if(that.getIsMouseDown()) { return 'buttonHMinusClicked'; }
            else if (that.getIsMouseOver()) { return 'buttonHMinusOver'; }
            else if (that.getIsDisabled()) { return 'buttonHMinusDisabled';	}
            else { return 'buttonHMinusNormal'; }
        };

        return that;
    };
    // make public
    classScope.createHMButtonComponent = createHMButtonComponent;

    // ------------------------
    // vertical up - button subclass
    var createUpButtonComponent = function(scene, text, w, h) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';

        var that = createButtonComponent(scene, text, w, h);

        // ------------------------
        // private methods

        // ------------------------
        // public methods

        that.getUIItemBaseName = function() {
            if(that.getIsMouseDown()) { return 'buttonUpClicked'; }
            else if (that.getIsMouseOver()) { return 'buttonUpOver'; }
            else if (that.getIsDisabled()) { return 'buttonUpDisabled';	}
            else { return 'buttonUpNormal'; }
        };

        return that;
    };
    // make public
    classScope.createUpButtonComponent = createUpButtonComponent;

    // ------------------------
    // vertical down - button subclass
    var createDownButtonComponent = function(scene, text, w, h) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';

        var that = createButtonComponent(scene, text, w, h);

        // ------------------------
        // private methods

        // ------------------------
        // public methods

        that.getUIItemBaseName = function() {
            if(that.getIsMouseDown()) { return 'buttonDownClicked'; }
            else if (that.getIsMouseOver()) { return 'buttonDownOver'; }
            else if (that.getIsDisabled()) { return 'buttonDownDisabled';	}
            else { return 'buttonDownNormal'; }
        };

        return that;
    };
    // make public
    classScope.createDownButtonComponent = createDownButtonComponent;

    // ------------------------
    // simpler ui button - button subclass
    var createSButtonComponent = function(scene, text, w, h) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';

        var that = createButtonComponent(scene, text, w, h);

        // ------------------------
        // private methods

        // ------------------------
        // public methods

        that.getUIItemBaseName = function() {
            if(that.getIsMouseDown()) { return 'label'; }
            else if (that.getIsMouseOver()) { return 'label2'; }
            else if (that.getIsDisabled()) { return 'label';	}
            else { return 'label3'; }
        };

        return that;
    };
    // make public
    classScope.createSButtonComponent = createSButtonComponent;


    // ------------------------
    // TextEdit subclass
    var createTextEditComponent = function(scene, w, h) {
        // ------------------------
        // private variables
        var that = createComponent(scene, 'default', '', w, h);
        var text = '';

        // ------------------------
        // private methods

        // ------------------------
        // public methods
        that.getText = function() {
            return text;
        };

        var setText = function(t) {
            text = t;
        };
        that.setText = setText;

        return that;
    };
    // make public
    classScope.createTextEditComponent = createTextEditComponent;

    // ------------------------
    // ValueEdit subclass
    var createValueEditComponent = function(scene, minVal, maxVal, val, w, h) {
        // ------------------------
        // private variables
        if(typeof(minVal)==='undefined') minVal = 1;
        if(typeof(maxVal)==='undefined') maxVal = 20;
        if(typeof(val)==='undefined') val = 10;

        var that = createTextEditComponent(scene, w, h);
        that.setText(''+val);

        // ------------------------
        // private methods

        // ------------------------
        // public methods
        var increase = function() {
            val = (val < maxVal) ? val + 1 : maxVal;
            that.setText(''+val);	
        };
        that.increase = increase;

        var decrease = function(t) {
            val = (val > minVal) ? val - 1 : minVal;
            that.setText(''+val);	
        };
        that.decrease = decrease;

        var getValue = function() {
            return val;
        };
        that.getValue = getValue;

        var setValue = function(v) {
            val = v;
            that.setText(''+val);
            return val;
        };
        that.setValue = setValue;

        return that;
    };
    // make public
    classScope.createValueEditComponent = createValueEditComponent;



    // ------------------------
    // horizontal - button subclass
    var createValueComponent = function(scene, text, minVal, maxVal, val, w, h, beforeCallback, afterCallback, align, xComp) {
        if(align !== 'left') align = 'center';
        if(typeof xComp === 'undefined') xComp = null;

        // ------------------------
        // private variables
        var that = createValueEditComponent(scene, minVal, maxVal, val, w, h);
        var editable = true;

        var m1 = csComponents.createHMButtonComponent(scene, '', w, h);
        that.addChild(m1);
        m1.onMouseUp = function() { 
           if(typeof beforeCallback === 'function') {
               if(beforeCallback(that.getValue(), that.getValue()-1) === false) {
                   return;
               }
           }
           var prev = that.getValue();
           that.decrease();
           that.paint();
           if(typeof afterCallback === 'function') {
               afterCallback(that.getValue(), prev, text);
           }
        };

        var p1 = csComponents.createHPButtonComponent(scene, '', w, h);
        that.addChild(p1);
        p1.onMouseUp = function() {
           if(typeof beforeCallback === 'function') {
               if(beforeCallback(that.getValue(), that.getValue()+1) === false) {
                   return;
               }
           }
           var prev = that.getValue();
           that.increase();
           that.paint();
           if(typeof afterCallback === 'function') {
               afterCallback(that.getValue(), prev, text);
           }
        };

        var lbl = null;
        if(typeof(text)==='undefined') {
            text = '';
        }
        else {
            lbl = csComponents.createTextEditComponent(scene, 270, h);
            lbl.align = align;
            lbl.setText(text);
            that.addChild(lbl);
        }

        if(xComp !== null) {
            that.addChild(xComp);
        }

        // ------------------------
        // private methods

        // ------------------------
        // public methods
        var superSetValue = that.setValue;
        that.setValue = function(v, notify) {
            if(notify !== false) {notify = true;}
            var ov = that.getValue();
            if(typeof beforeCallback === 'function') {
               if(notify && beforeCallback(ov, v) === false) {
                   return ov;
               }
            }

           var nv = superSetValue(v);

           if(typeof afterCallback === 'function') {
               if(notify) {
                    afterCallback(nv, ov, text);
               }
           }

           return nv;
        };

        var superSetPosition = that.setPosition;
        that.setPosition = function(x, y, z) {
            superSetPosition(x, y, z);
            m1.setPosition(x-m1.getSizeX(), y, z);
            p1.setPosition(x+that.getSizeX(), y, z);
            var xOff = 15;
            if(xComp !== null) {
                xOff += xComp.getSizeX();
            }
            if(lbl !== null) {
                lbl.setPosition(x - lbl.getSizeX() * 0.5 - xOff - 1.5*that.getSizeX(), y, z);
            }
            if(xComp !== null) {
                xComp.setPosition( x - 10 - xComp.getSizeX() - that.getSizeX(), y, z);
            }
        };

        var superSetSize = that.setSize;
        that.setSize = function(x, y, z) {
            superSetSize(x, y, z);
            var sz = (x < y) ? x : y;
            m1.setSize(sz, sz, z);
            p1.setSize(sz, sz, z);
        };

        var setEditable = function(e) {
            editable = e;
            if(e && that.isVisible()) {
                m1.addToScene();
                p1.addToScene();
            }
            else if (!e && that.isVisible()) {
                m1.removeFromScene();
                p1.removeFromScene();
            }
            m1.setDisabled(!e);
            p1.setDisabled(!e);
            that.paint();
        };
        that.setEditable = setEditable;

        return that;
    };
    // make public
    classScope.createValueComponent = createValueComponent;


    var createValueListComponent = function(scene, text, list, val, w, h, beforeCallback, afterCallback) {
        // ------------------------
        // private variables
        var that = createTextEditComponent(scene, w, h);
        that.setText(list[val]);

        var m1 = csComponents.createHMButtonComponent(scene, text[val], w, h);
        that.addChild(m1);
        m1.onMouseUp = function() { 
           var nVal = (val - 1 + list.length)%list.length;
           if(typeof beforeCallback === 'function') {
               if(beforeCallback(list[val], list[nVal]) === false) {
                   return;
               }
           }
           var prev = list[val];
           val = nVal;
           that.setText(list[val]);
           that.paint();
           if(typeof afterCallback === 'function') {
               afterCallback(list[val], list[prev]);
           }
        };

        var p1 = csComponents.createHPButtonComponent(scene, '', w, h);
        that.addChild(p1);
        p1.onMouseUp = function() {
           var nVal = (val + 1)%list.length;
           if(typeof beforeCallback === 'function') {
               if(beforeCallback(list[val], list[nVal]) === false) {
                   return;
               }
           }
           var prev = list[val];
           val = nVal;
           that.setText(list[val]);
           that.paint();
           if(typeof afterCallback === 'function') {
               afterCallback(list[val], list[prev]);
           }
        };

        var lbl = null;
        if(typeof(text)==='undefined') {
            text = '';
        }
        else {
            lbl = csComponents.createTextEditComponent(scene, 350, h);
            lbl.setText(text);
            that.addChild(lbl);
        }

        // ------------------------
        // private methods

        // ------------------------
        // public methods
        var superSetPosition = that.setPosition;
        that.setPosition = function(x, y, z) {
            superSetPosition(x, y, z);
            m1.setPosition(x-(that.getSizeX()+m1.getSizeX())*0.5, y, z);
            p1.setPosition(x+(that.getSizeX()+p1.getSizeX())*0.5, y, z);
            if(lbl !== null) {
                lbl.setPosition(x-lbl.getSizeX()*0.5, y, z);
            }
        };

        var superSetSize = that.setSize;
        that.setSize = function(x, y, z) {
            superSetSize(x, y, z);
            var sz = (x < y) ? x : y;
            m1.setSize(sz, sz, z);
            p1.setSize(sz, sz, z);
        };

        var setEditable = function(e) {
            editable = e;
            if(e && that.isVisible()) {
                m1.addToScene();
                p1.addToScene();
            }
            else if (!e && that.isVisible()) {
                m1.removeFromScene();
                p1.removeFromScene();
            }
            m1.setDisabled(!e);
            p1.setDisabled(!e);
            that.paint();
        };
        that.setEditable = setEditable;

        return that;
    };
    // make public
    classScope.createValueListComponent = createValueListComponent;


    // ------------------------
    // list subclass
    var createListComponent = function(scene, text, w, h) {
        // ------------------------
        // private variables
        if(typeof(text)==='undefined') text = '';

        var that = createComponent(scene, text, '', w, h);

        var m1 = csComponents.createUpButtonComponent(scene, '', 32, 32);
        that.addChild(m1);
        m1.onMouseUp = function() { moveList(topIndex-1); that.paint(); };

        var p1 = csComponents.createDownButtonComponent(scene, '', 32, 32);
        that.addChild(p1);
        p1.onMouseUp = function() { moveList(topIndex+1); that.paint(); };

        // add contents
        var topIndex = 0;
        var contents = [];

        // ------------------------
        // private methods
              
        // ------------------------
        // public methods
        
        var moveList = function(newTop) {
            if (newTop >= 0 && newTop < contents.length) {
                topIndex = newTop;
            }
            var margin = 5;
            var rMargin = 40;
            var cumY = margin + ((contents.length > 0) ? contents[0].getSizeY()*0.5 : 0);
            for(var i=0; i<contents.length; i++) {
                var comp = contents[i];
                if(cumY + comp.getSizeY() * 0.5 > that.getSizeY() || i < topIndex) {
                    comp.removeFromScene();
                }
                else {
                    if(that.isVisible()) {
                        comp.addToScene();
                    }
                    comp.setPosition(that.getPositionX()+(that.getSizeX()-comp.getSizeX())*0.5-rMargin, that.getPositionY()+that.getSizeY()*0.5-cumY, (that.getPositionZ()+1 || 1));
                    cumY += comp.getSizeY();
                }
            }
        };
        that.moveList = moveList;

        var setContents = function(l) {
            that.removeAllChildren([m1, p1]);

            for(var c=0; c<l.length; c++) {
                that.addChild(l[c]);
            }
            contents = l;
            moveList(topIndex);
        }; 
        that.setContents = setContents;
 //       setContents(contents);

        // overriden  methods
        
        var superSetPosition = that.setPosition;
        that.setPosition = function(x, y, z) {
            superSetPosition(x, y, z);
            m1.setPosition(x+(w+m1.getSizeX())*0.5, y+(that.getSizeY()-m1.getSizeX())*0.5, z);
            p1.setPosition(x+(w+p1.getSizeX())*0.5, y-(that.getSizeY()-p1.getSizeY())*0.5, z);
            moveList(topIndex);
        };

        var superSetSize = that.setSize;
        that.setSize = function(x, y, z) {
            superSetSize(x, y, z);
            var sz = (x < y) ? x : y;
        };

        var superAddToScene = that.addToScene;
        var addToScene = function() {
            superAddToScene();
            moveList(topIndex);
        };
        that.addToScene = addToScene;

        return that;
    };
    // make public
    classScope.createListComponent = createListComponent;


    // ------------------------
    // load gui resources
    backgroundTexture.onload = imageHasLoaded;
    backgroundPaperTexture.onload = imageHasLoaded;
    backgroundInventoryTexture.onload = imageHasLoaded;
    iconsTexture.onload = imageHasLoaded;
    backgroundTexture.src = 'textures/gui/gui.png';
    backgroundPaperTexture.src = 'textures/gui/paper.png';
    backgroundInventoryTexture.src = 'textures/gui/inventory.png';
    iconsTexture.src = 'textures/gui/icons.png';


    return classScope;
}();

