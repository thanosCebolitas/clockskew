// TODO: all sizes should be relative

// ================================================
// start screen 
function createMainScreen(scene) {
    var gui = csComponents.createComponent(scene, 'paper', '', three.CANVAS_WIDTH, three.CANVAS_HEIGHT);

    csComponents.addChild(gui);
    gui.setPosition(three.CANVAS_WIDTH * 0.5, three.CANVAS_HEIGHT * 0.5, -2);

    var titleLabel = csComponents.createComponent(scene, 'default', 'Clock Skew', 150, 40);
    gui.addChild(titleLabel);
    titleLabel.setPosition(three.CANVAS_WIDTH * 0.5, gGY(-1.5, 20), 1);

    var b1 = csComponents.createButtonComponent(scene, 'New Game', 290, 67);
    gui.addChild(b1);
    b1.setPosition(three.CANVAS_WIDTH * 0.5, gGY(6, 20), 1);        
    b1.onMouseUp = function () {
        gui.removeFromScene();
        characterSheet = createCharacterCreationScreen(three.uiScene, gui, null, showAllGameObjects); 
        characterSheet.addToScene();
    };

    var b2 = csComponents.createButtonComponent(scene, 'Load Game', 290, 67);
    b2.setDisabled(true);
    gui.addChild(b2);
    b2.setPosition(three.CANVAS_WIDTH * 0.5, gGY(8, 20), 1);
    b2.onMouseUp = function () { alert("TODO"); };

    var b3 = csComponents.createButtonComponent(scene, 'Save Game', 290, 67);
    b3.setDisabled(true);
    gui.addChild(b3);
    b3.setPosition(three.CANVAS_WIDTH * 0.5, gGY(10, 20), 1);
    b3.onMouseUp = function () { alert("TODO"); };

    var l1 = csComponents.createComponent(scene, 'default', '- fortune coockie -', 400, 300);
    gui.addChild(l1);
    l1.setPosition(three.CANVAS_WIDTH * 0.5, gGY(21.5, 20), 1);

    return gui;
}

// ================================================
// character sheet 
function createCharacterCreationScreen(scene, backCmp, charSheet, endClbk) {
    var gui = csComponents.createComponent(scene, 'paper', '', three.CANVAS_WIDTH, three.CANVAS_HEIGHT);
    // TODO: this keeps the screen in memory, find a solution (elsewhere)
    csComponents.addChild(gui);
    gui.setPosition(three.CANVAS_WIDTH * 0.5, three.CANVAS_HEIGHT * 0.5, -2);

    var titleLabel = csComponents.createComponent(scene, 'default', 'Clock Skew', 150, 40);
    gui.addChild(titleLabel);
    titleLabel.setPosition(three.CANVAS_WIDTH * 0.5, gGY(-1.5, 20), 1);

    // shortcut
    var cs = csR.csCreateCharacter(charSheet);

    // ================================================
    // STATS
    
    var upRA = function(v, p) {
        cs.remainingAttributes = cs.remainingAttributes - v + p; 
        remAbVal.setText(''+cs.remainingAttributes);
        remAbVal.paint();

        for(var sk in cs.Skills) {
            updateSKV(sk);
        }
    };

    var bStatClbk = function(v, p) {
        return v > p || cs.remainingAttributes > 0;
    };

    var strClbk = function(v, p) {
        cs.Str[1] = v;
        var diff = Math.floor((v-10)/2) - Math.floor((p-10)/2);
        cs.Melee[1] = cs.Melee[1] + diff; 
        vStr2.setText((v<10?'':'+')+Math.floor((v-10)/2)); vStr2.paint();
        meleeVal.setText((cs.Melee[1]<0?'':'+')+cs.Melee[1]); meleeVal.paint();
        upRA(v, p);
    };
    var dexClbk = function(v, p) {
        cs.Dex[1] = v;
        var diff = Math.floor((v-10)/2) - Math.floor((p-10)/2);
        cs.Initiative[1] = cs.Initiative[1] + diff;
        cs.Reflex[1] = cs.Reflex[1] + diff; 
        cs.AC[1] = cs.AC[1] + diff; // TODO plus armor/other limits
        cs.Ranged[1] = cs.Ranged[1] + diff; 
        vDex2.setText((v<10?'':'+')+Math.floor((v-10)/2)); vDex2.paint();
        initVal.setText((cs.Initiative[1]<0?'':'+')+cs.Initiative[1]); initVal.paint();
        vRef.setText((cs.Reflex[1]<0?'':'+')+cs.Reflex[1]); vRef.paint();
        acVal.setText(cs.AC[1]); acVal.paint();
        rangedVal.setText((cs.Ranged[1]<0?'':'+')+cs.Ranged[1]); rangedVal.paint();
        upRA(v, p);
    };
    var conClbk = function(v, p) {
        cs.Con[1] = v;
        var diff = Math.floor((v-10)/2) - Math.floor((p-10)/2);
        cs.Fortitude[1] = cs.Fortitude[1] + diff;
        var cl = cs.Class[1][0]+cs.Class[1][1]+cs.Class[1][2];
        cs.HP[1] = cs.HP[1] + diff*2*cl; 
        vCon2.setText((v<10?'':'+')+Math.floor((v-10)/2)); vCon2.paint();
        vFort.setText((cs.Fortitude[1]<0?'':'+')+cs.Fortitude[1]); vFort.paint();
        hpVal.setText(cs.HP[1]); hpVal.paint();
        upRA(v, p);
    };
    var intClbk = function(v, p) {
        cs.Int[1] = v;
        cs.updateRemSkills(classVal.getText(), Math.floor((v-10)/2), Math.floor((p-10)/2));
        remSkVal.setText(''+cs.remainingSkills);
        remSkVal.paint();
        vInt2.setText((v<10?'':'+')+Math.floor((v-10)/2)); vInt2.paint();
        upRA(v, p);

        if(cs.remainingSkills < 0) {
            // TODO: error
            vInt.setValue(p);           
            vInt.paint();
        }
    };
    var wisClbk = function(v, p) {
        cs.Wis[1] = v;
        var diff = Math.floor((v-10)/2) - Math.floor((p-10)/2);
        cs.Will[1] = cs.Will[1] + diff;
        vWis2.setText((v<10?'':'+')+Math.floor((v-10)/2)); vWis2.paint();
        vWill.setText((cs.Will[1]<0?'':'+')+cs.Will[1]); vWill.paint();
        upRA(v, p);
    };
    var chaClbk = function(v, p) {
        cs.Cha[1] = v;
        vCha2.setText((v<10?'':'+')+Math.floor((v-10)/2)); vCha2.paint();
        upRA(v, p);
    };
    var getMod = function(v) {
        return (v<10?'':'+')+Math.floor((v-10)/2);
    };

    var l1 = (cs.Class[1][0]+cs.Class[1][1]+cs.Class[1][2] === 0);
    var vStr = csComponents.createValueComponent(scene, '', l1?3:cs.Str[1], 20, cs.Str[1], 32, 32, bStatClbk, strClbk);
    var vDex = csComponents.createValueComponent(scene, '', l1?3:cs.Dex[1], 20, cs.Dex[1], 32, 32, bStatClbk, dexClbk);
    var vCon = csComponents.createValueComponent(scene, '', l1?3:cs.Con[1], 20, cs.Con[1], 32, 32, bStatClbk, conClbk);
    var vInt = csComponents.createValueComponent(scene, '', l1?3:cs.Int[1], 20, cs.Int[1], 32, 32, bStatClbk, intClbk);
    var vWis = csComponents.createValueComponent(scene, '', l1?3:cs.Wis[1], 20, cs.Wis[1], 32, 32, bStatClbk, wisClbk);
    var vCha = csComponents.createValueComponent(scene, '', l1?3:cs.Cha[1], 20, cs.Cha[1], 32, 32, bStatClbk, chaClbk);
    gui.addChild(vStr);
    gui.addChild(vDex);
    gui.addChild(vCon);
    gui.addChild(vInt);
    gui.addChild(vWis);
    gui.addChild(vCha);
    var vStr2 = csComponents.createTextEditComponent(scene, 32, 32);
    var vDex2 = csComponents.createTextEditComponent(scene, 32, 32);
    var vCon2 = csComponents.createTextEditComponent(scene, 32, 32);
    var vInt2 = csComponents.createTextEditComponent(scene, 32, 32);
    var vWis2 = csComponents.createTextEditComponent(scene, 32, 32);
    var vCha2 = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(vStr2);
    gui.addChild(vDex2);
    gui.addChild(vCon2);
    gui.addChild(vInt2);
    gui.addChild(vWis2);
    gui.addChild(vCha2);

    var maxcols = 20;
    var maxrows = 19;
    var labelX = gGX(1, maxcols);
    var vX = gGX(5, maxcols);
    var vX2 = gGX(7, maxcols);

    var strengthLabel = csComponents.createComponent(scene, 'label', 'Strength', 150, 32);
    gui.addChild(strengthLabel);
    strengthLabel.setPosition(labelX, gGY(3.5, maxrows), 1);
    vStr.setPosition(vX, gGY(3.5, maxrows), 1);
    vStr2.setPosition(vX2, gGY(3.5, maxrows), 1);

    var dexLabel = csComponents.createComponent(scene, 'label', 'Dexterity', 150, 32);
    gui.addChild(dexLabel);
    dexLabel.setPosition(labelX, gGY(4.5, maxrows), 1);
    vDex.setPosition(vX, gGY(4.5, maxrows), 1);
    vDex2.setPosition(vX2, gGY(4.5, maxrows), 1);

    var conLabel = csComponents.createComponent(scene, 'label', 'Constitution', 150, 32);
    gui.addChild(conLabel);
    conLabel.setPosition(labelX, gGY(5.5, maxrows), 1);
    vCon.setPosition(vX, gGY(5.5, maxrows), 1);
    vCon2.setPosition(vX2, gGY(5.5, maxrows), 1);

    var intLabel = csComponents.createComponent(scene, 'label', 'Intelligence', 150, 32);
    gui.addChild(intLabel);
    intLabel.setPosition(labelX, gGY(6.5, maxrows), 1);
    vInt.setPosition(vX, gGY(6.5, maxrows), 1);
    vInt2.setPosition(vX2, gGY(6.5, maxrows), 1);

    var wisLabel = csComponents.createComponent(scene, 'label', 'Wisdom', 150, 32);
    gui.addChild(wisLabel);
    wisLabel.setPosition(labelX, gGY(7.5, maxrows), 1);
    vWis.setPosition(vX, gGY(7.5, maxrows), 1);
    vWis2.setPosition(vX2, gGY(7.5, maxrows), 1);

    var charLabel = csComponents.createComponent(scene, 'label', 'Charisma', 150, 32);
    gui.addChild(charLabel);
    charLabel.setPosition(labelX, gGY(8.5, maxrows), 1);
    vCha.setPosition(vX, gGY(8.5, maxrows), 1);
    vCha2.setPosition(vX2, gGY(8.5, maxrows), 1);

    var remAbLabel = csComponents.createComponent(scene, 'label', 'Rem. Ability Points', 250, 32);
    var remAbVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(remAbLabel);
    gui.addChild(remAbVal);
    remAbLabel.setPosition(gGX(2, maxcols), gGY(9.75, maxrows), 1);
    remAbVal.setPosition(gGX(6, maxcols), gGY(9.75, maxrows), 1);

    // ================================================
    // basic info  

    labelX = gGX(9, maxcols);
    vX = gGX(12, maxcols);

    var hpLabel = csComponents.createComponent(scene, 'label', 'HP', 150, 32);
    var hpVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(hpLabel);
    gui.addChild(hpVal);
    hpLabel.setPosition(labelX, gGY(3.5, maxrows), 1);
    hpVal.setPosition(vX, gGY(3.5, maxrows), 1);

    var acLabel = csComponents.createComponent(scene, 'label', 'AC', 150, 32);
    var acVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(acLabel);
    gui.addChild(acVal);
    acLabel.setPosition(labelX, gGY(4.5, maxrows), 1);
    acVal.setPosition(vX, gGY(4.5, maxrows), 1);

    var spdLabel = csComponents.createComponent(scene, 'label', 'Speed', 150, 32);
    var spdVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(spdLabel);
    gui.addChild(spdVal);
    spdLabel.setPosition(labelX, gGY(6, maxrows), 1);
    spdVal.setPosition(gGX(12, maxcols), gGY(6, maxrows), 1);

    var initLabel = csComponents.createComponent(scene, 'label', 'Initiative', 150, 32);
    var initVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(initLabel);
    gui.addChild(initVal);
    initLabel.setPosition(labelX, gGY(7, maxrows), 1);
    initVal.setPosition(gGX(12, maxcols), gGY(7, maxrows), 1);

    var babLabel = csComponents.createComponent(scene, 'label', 'BAB', 150, 32);
    var babVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(babLabel);
    gui.addChild(babVal);
    babLabel.setPosition(labelX, gGY(8, maxrows), 1);
    babVal.setPosition(gGX(12, maxcols), gGY(8, maxrows), 1);

    labelX = gGX(15, maxcols);
    vX = gGX(19, maxcols);

    var meleeLabel = csComponents.createComponent(scene, 'label', 'Melee', 150, 32);
    var meleeVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(meleeLabel);
    gui.addChild(meleeVal);
    meleeLabel.setPosition(labelX, gGY(3.5, maxrows), 1);
    meleeVal.setPosition(vX, gGY(3.5, maxrows), 1);

    var rangedLabel = csComponents.createComponent(scene, 'label', 'Ranged', 150, 32);
    var rangedVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(rangedLabel);
    gui.addChild(rangedVal);
    rangedLabel.setPosition(labelX, gGY(4.5, maxrows), 1);
    rangedVal.setPosition(vX, gGY(4.5, maxrows), 1);

    // ================================================
    // saves
 
    var fortLabel = csComponents.createComponent(scene, 'label', 'Fortitude', 150, 32);
    var vFort = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(fortLabel);
    gui.addChild(vFort);
    fortLabel.setPosition(labelX, gGY(6, maxrows), 1);
    vFort.setPosition(vX, gGY(6, maxrows), 1);

    var refLabel = csComponents.createComponent(scene, 'label', 'Reflex', 150, 32);
    var vRef = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(vRef);
    gui.addChild(refLabel);
    refLabel.setPosition(labelX, gGY(7, maxrows), 1);
    vRef.setPosition(vX, gGY(7, maxrows), 1);

    var willLabel = csComponents.createComponent(scene, 'label', 'Will', 150, 32);
    var vWill = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(willLabel);
    gui.addChild(vWill);
    willLabel.setPosition(labelX, gGY(8, maxrows), 1);
    vWill.setPosition(vX, gGY(8, maxrows), 1);

    // ================================================
    // player info 

    labelX = gGX(1, maxcols);
    vX = gGX(5, maxcols);

    var nameLabel = csComponents.createComponent(scene, 'label', 'Name', 150, 32);
    var nameVal = csComponents.createTextEditComponent(scene, 150, 32);
    gui.addChild(nameLabel);
    gui.addChild(nameVal);
    nameLabel.setPosition(gGX(4, maxcols), gGY(0, maxrows), 1);
    nameVal.setPosition(gGX(8, maxcols), gGY(0, maxrows), 1);

    var ageLabel = csComponents.createComponent(scene, 'label', 'Age', 150, 32);
    var ageVal = csComponents.createValueComponent(scene, '', 16, 119, 16, 32, 32);
    gui.addChild(ageLabel);
    gui.addChild(ageVal);
    ageLabel.setPosition(labelX, gGY(1, maxrows), 1);
    ageVal.setPosition(vX, gGY(1, maxrows), 1);

    var genderLabel = csComponents.createComponent(scene, 'label', 'Gender', 150, 32);
    var genderVal = csComponents.createValueListComponent(scene, '', ['Female', 'Male'], 0, 80, 32);
    gui.addChild(genderLabel);
    gui.addChild(genderVal);
    genderLabel.setPosition(labelX, gGY(2, maxrows), 1);
    genderVal.setPosition(gGX(5, maxcols), gGY(2, maxrows), 1);

    labelX = gGX(9, maxcols);
    vX = gGX(12, maxcols);

    var algnList = ['LG', 'NG', 'CG', 'LN', 'NN', 'CN', 'LE', 'NE', 'CE'];
    var algnLabel = csComponents.createComponent(scene, 'label', 'Alignment', 150, 32);
    var algnVal = csComponents.createValueListComponent(scene, '', algnList, 4, 80, 32);
    gui.addChild(algnLabel);
    gui.addChild(algnVal);
    algnLabel.setPosition(labelX, gGY(1, maxrows), 1);
    algnVal.setPosition(vX, gGY(1, maxrows), 1);

    var deityList = ['Mahat', 'Agnostic', 'Atheist'];
    var deityLabel = csComponents.createComponent(scene, 'label', 'Deity', 150, 32);
    var deityVal = csComponents.createValueListComponent(scene, '', deityList, 0, 80, 32);
    gui.addChild(deityLabel);
    gui.addChild(deityVal);
    deityLabel.setPosition(labelX, gGY(2, maxrows), 1);
    deityVal.setPosition(gGX(12, maxcols), gGY(2, maxrows), 1);

    labelX = gGX(15, maxcols);
    vX = gGX(19, maxcols);

    var resetCS = function() {
        cs = csR.csCreateCharacter(three.characterSheet);
        cs.addClassToCharacterSheet(classVal.getText());

        updateSheet(cs);
        skillsList.setContents(createSkillListComponents(cs));
        featsList.setContents(createFeatsListComponents(cs));
    };
    gui.resetCS = resetCS;

    var selectClass = function(c, n) {
        cs = csR.csCreateCharacter(three.characterSheet);
        cs.addClassToCharacterSheet(n);
        updateSheet(cs);
        skillsList.setContents(createSkillListComponents(cs));
        featsList.setContents(createFeatsListComponents(cs));
    };

    var classLabel = csComponents.createComponent(scene, 'label', 'Class', 150, 32);
    var classVal = csComponents.createValueListComponent(scene, '', ['Fighter', 'Barbarian', 'Thief'], 0, 100, 32, selectClass);
    gui.addChild(classLabel);
    gui.addChild(classVal);
    classLabel.setPosition(labelX, gGY(0, maxrows), 1);
    classVal.setPosition(gGX(19, maxcols), gGY(0, maxrows), 1);

    var lvlLabel = csComponents.createComponent(scene, 'label', 'Level', 150, 32);
    var lvlVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(lvlLabel);
    gui.addChild(lvlVal);
    lvlLabel.setPosition(labelX, gGY(1, maxrows), 1);
    lvlVal.setPosition(vX, gGY(1, maxrows), 1);

    var xpLabel = csComponents.createComponent(scene, 'label', 'Experience', 150, 32);
    var xpVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(xpLabel);
    gui.addChild(xpVal);
    xpLabel.setPosition(labelX, gGY(2, maxrows), 1);
    xpVal.setPosition(vX, gGY(2, maxrows), 1);

    var remSkLabel = csComponents.createComponent(scene, 'label', 'Rem. Skill Points', 250, 32);
    var remSkVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(remSkLabel);
    gui.addChild(remSkVal);
    remSkLabel.setPosition(gGX(2, maxcols), gGY(17, maxrows), 1);
    remSkVal.setPosition(gGX(6, maxcols), gGY(17, maxrows), 1);

    var remFtLabel = csComponents.createComponent(scene, 'label', 'Rem. Feats', 250, 32);
    var remFtVal = csComponents.createTextEditComponent(scene, 32, 32);
    gui.addChild(remFtLabel);
    gui.addChild(remFtVal);
    remFtLabel.setPosition(gGX(12, maxcols), gGY(17, maxrows), 1);
    remFtVal.setPosition(gGX(16, maxcols), gGY(17, maxrows), 1);


    // ================================================
    // usefull functions
    var totalSK = {};
    var updateSKV = function(sk) {
        // update total
        // total = ranks + ability + synergy
        var src = (cs.Skills[sk])[5];
        var syn = ( cs.Skills[src] && cs.Skills[src][1] >= 5 ) ? 2 : 0;
        var ab = cs.Skills[sk][2];
        if (cs[ab] === undefined) {
            return;
        }
        var totalVal = cs.Skills[sk][1] + Math.floor( ( cs[ab][1] - 10 ) * 0.5 ) + syn;

        for (var ft in cs.Feats) {
            // skill-feat application
            if(cs.Feats[ft].isSelected && cs.Feats[ft].hasOwnProperty('Skills') &&
                cs.Feats[ft].Skills.hasOwnProperty(sk)) {
                    totalVal += cs.Feats[ft].Skills[sk];
            }
        }

        totalSK[sk].setText(totalVal);
        totalSK[sk].paint();
    };

    var createSkillListComponents = function(cs) {
        var sl = [];
        var charLevel = cs.Class[1][0]+cs.Class[1][1]+cs.Class[1][2];

        var bSkillClbk = function(v, p) {
            return v > p || cs.remainingSkills > 0;
        };

        var upRSP = function(v, p, sk) {
            cs.Skills[sk][1] = v;
            cs.remainingSkills = cs.remainingSkills - v + p; 
            remSkVal.setText(''+cs.remainingSkills);
            remSkVal.paint();

            for(var ask in cs.Skills) {
                updateSKV(sk);
            }
        };

        // skills-components
        for(var sk in cs.Skills) {
            var xComp = csComponents.createTextEditComponent(scene, 32, 32);
            totalSK[sk] = xComp;
            updateSKV(sk);
            sl[sl.length] = csComponents.createValueComponent(scene, sk, cs.Skills[sk][1], charLevel+3, cs.Skills[sk][1], 32, 32, bSkillClbk, upRSP, 'left', xComp);
        }

        return sl;
    };

    var createFeatsListComponents = function(cs) {
        var bFeatsClbk = function(v) {
            return v || cs.remainingFeats > 0;
        };

        var upRF = function(v, p, ft) {
            cs.Feats[ft].isSelected = v;
            cs.remainingFeats = cs.remainingFeats + (v?-1:1); 
            remFtVal.setText(''+cs.remainingFeats);
            remFtVal.paint();

            if(cs.Feats[ft].hasOwnProperty('Skills')) {
                for(var sk in cs.Feats[ft].Skills) {
                    updateSKV(sk);
                }
            }
        };

       // feats-components
        var fl = [];
        for(var fk in cs.Feats) {
            var chkbx = csComponents.createCheckBoxComponent(scene, fk, 32, 32, bFeatsClbk, upRF, cs.Feats[fk].isSelected);
            chkbx.setDisabled(cs.Feats[fk].isSelected);
            fl[fl.length] = chkbx; 
        }

        return fl;
    };

    // ================================================
    // SKILLS
    var sl = createSkillListComponents(cs);

    // the list component
    var skillsList = csComponents.createListComponent(scene, 'label2', 450, 170);
    skillsList.setContents(sl);
    gui.addChild(skillsList);
    skillsList.setPosition(gGX(4, maxcols), gGY(13.5, maxrows), -1);

    // ================================================
    // FEATS
    var fl = createFeatsListComponents(cs);

    // the list component
    var featsList = csComponents.createListComponent(scene, 'label2', 450, 170);
    gui.addChild(featsList);
    featsList.setContents(fl);
    featsList.setPosition(gGX(14, maxcols), gGY(13.5, maxrows), -1);

    var updateSheet = function(ncs) {
        vStr.setValue(ncs.Str[1], false);
        vStr2.setText(getMod(ncs.Str[1]));
        vStr.paint();
        vStr2.paint();
        vDex.setValue(ncs.Dex[1], false);
        vDex2.setText(getMod(ncs.Dex[1]));
        vDex.paint();
        vDex2.paint();
        vCon.setValue(ncs.Con[1], false);
        vCon2.setText(getMod(ncs.Con[1]));
        vCon.paint();
        vCon2.paint();
        vInt.setValue(ncs.Int[1], false);
        vInt2.setText(getMod(ncs.Int[1]));
        vInt.paint();
        vInt2.paint();
        vWis.setValue(ncs.Wis[1], false);
        vWis2.setText(getMod(ncs.Wis[1]));
        vWis.paint();
        vWis2.paint();
        vCha.setValue(ncs.Cha[1], false);
        vCha2.setText(getMod(ncs.Cha[1]));
        vCha.paint();
        vCha2.paint();
        remAbVal.setText(''+ncs.remainingAttributes);
        remAbVal.paint();
        babVal.setText((ncs.BAB[1]<0?'':'+')+ncs.BAB[1]);
        babVal.paint();

        hpVal.setText(ncs.HP[1]);
        hpVal.paint();
        acVal.setText(ncs.AC[1]);
        acVal.paint();
        spdVal.setText(ncs.Speed[1]);
        spdVal.paint();
        initVal.setText(ncs.Initiative[1]);
        initVal.paint();
        meleeVal.setText((ncs.Melee[1]<0?'':'+')+ncs.Melee[1]);
        meleeVal.paint();
        rangedVal.setText((ncs.Ranged[1]<0?'':'+')+ncs.Ranged[1]);
        rangedVal.paint();
        vFort.setText((ncs.Fortitude[1]<0?'':'+')+ncs.Fortitude[1]);
        vFort.paint();
        vRef.setText((ncs.Reflex[1]<0?'':'+')+ncs.Reflex[1]);
        vRef.paint();
        vWill.setText((ncs.Will[1]<0?'':'+')+ncs.Will[1]);
        vWill.paint();
        nameVal.setText(ncs.Name);
        nameVal.paint();
        lvlVal.setText(ncs.Class[1][0]+ncs.Class[1][1]+ncs.Class[1][2]);
        lvlVal.paint();
        xpVal.setText(ncs.XP[1]);
        xpVal.paint();
        remSkVal.setText(''+ncs.remainingSkills);
        remSkVal.paint();
        remFtVal.setText(''+ncs.remainingFeats);
        remFtVal.paint();

        var en = ((cs.Class[1][0]+cs.Class[1][1]+cs.Class[1][2]) <= 1);
        genderVal.setEditable(en);
        ageVal.setEditable(en);
        algnVal.setEditable(en);
        deityVal.setEditable(en);

        for(var sk in cs.Skills) {
            updateSKV(sk);
        }
    };

    // ================================================
    // update all the above
    resetCS();

    // ================================================
    // sheet controls
    var btnText = charSheet ? 'Done' : 'Create';
    var b1 = csComponents.createButtonComponent(scene, btnText, 290, 67);
    gui.addChild(b1);
    b1.setPosition(gGX(16, maxcols), gGY(19, maxrows), 1);     
    b1.onMouseUp = function () {
       var charLevel = cs.Class[1][0]+cs.Class[1][1]+cs.Class[1][2];
       if(charLevel <= 20) {
           three.characterSheet = csR.csCreateCharacter(cs);

           gui.removeFromScene();
           if (isFunction(endClbk)) {
               endClbk();
           }
       }
       else {
           gui.removeFromScene();
       }
    };

    var b2 = csComponents.createButtonComponent(scene, 'Reset', 290, 67);
    gui.addChild(b2);
    b2.setPosition(gGX(10, maxcols), gGY(19, maxrows), 1);     
    b2.onMouseUp = function () { resetCS(); };

    var b3 = csComponents.createButtonComponent(scene, 'Back', 290, 67);
    gui.addChild(b3);
    b3.setPosition(gGX(4, maxcols), gGY(19, maxrows), 1);     
    b3.onMouseUp = function () { 
        gui.removeFromScene();
        if(backCmp) {
            backCmp.addToScene();
        }
        else if (isFunction(endClbk)) {
            endClbk();
        }
    }; 

    return gui;
}

// ================================================
// inventory screen 
function createInventoryScreen(scene, parentComp, charSheet, endClbk) {
    var gui = csComponents.createComponent(scene, 'paper', '', three.CANVAS_WIDTH, three.CANVAS_HEIGHT);

    var maxcols = 20;
    var maxrows = 19;

    csComponents.addChild(gui);
    gui.setPosition(three.CANVAS_WIDTH * 0.5, three.CANVAS_HEIGHT * 0.5, -2);

    var titleLabel = csComponents.createComponent(scene, 'default', 'Clock Skew', 150, 40);
    gui.addChild(titleLabel);
    titleLabel.setPosition(three.CANVAS_WIDTH * 0.5, gGY(-1.5, maxcols), 1);


    var bg = csComponents.createComponent(scene, 'inventorybg', 'bg', 280, 388);
    //gui.addChild(bg);
    bg.setPosition((three.CANVAS_WIDTH ) * 0.5, (three.CANVAS_HEIGHT + 200) * 0.5, -1);

    // backpack
    var inv = [];
    var invItems = [];
    var startx = gGX(6, maxrows);
    var xPos = startx;
    var starty1 = gGY(15, maxcols);
    var starty2 = gGY(16.5, maxcols);
    var yPos = starty1;

    var tf = function(that) {
        var comp = csComponents.getComponentFromXY(that.getPositionX(), that.getPositionY(), that, 'canSnapThat');
        if (comp && comp !== that) {
            comp.snapThat(that);
        }
        else {
            that.setPositionBeforeDragStart();
        }
    };

    var snapOD = function(other) {
        return true;
    };

    var cs = charSheet || three.characterSheet;
    for(var ind=0; ind<18; ind++) {
        // create slots
        inv[ind] = csComponents.createComponent(scene, 'invItem', '', 45, 45);
        inv[ind].snapCondition = snapOD;
        gui.addChild(inv[ind]);
        inv[ind].setPosition(xPos, yPos, 1);

        // create items if needed
        if(ind < cs.Inventory.length) {
            invItems[ind] = csComponents.createDraggableComponent(scene, 'invEqItem', '', 32, 32, tf);
            // setIcon
            invItems[ind].setUIItemBaseName( Items[ cs.Inventory[ ind ] ].Icon );
            // TODO: setItemReference
           
            // add and set position 
            gui.addChild(invItems[ind]);
            invItems[ind].setPosition(xPos, yPos, 2);
        }

        // pos update
        xPos += 46;
        xPos = (ind == 8) ? startx : xPos;
        yPos = (ind >= 8) ? starty2 : starty1; 
    }

    // equipped
    var eqInv = [];
    var maxEq = 6;
    var d8 = 2 * Math.PI / maxEq;
    var start8 = Math.PI / 2;
    var end8 = 2 * Math.PI + start8;
    var a = 140;
    var b = 180;

    for(ind = 0; ind < maxEq; ind++ ) {
        var theta = start8 + d8 * ind;
        xPos = a * Math.cos(theta) + three.CANVAS_WIDTH * 0.5;
        yPos = b * Math.sin(theta) + (three.CANVAS_HEIGHT + 200) * 0.5;
        eqInv[ind] = csComponents.createComponent(scene, 'invEqItem', '', 45, 45);
        eqInv[ind].snapCondition = snapOD; 
        gui.addChild(eqInv[ind]);
        eqInv[ind].setPosition(xPos, yPos, 1);
    }

    var dh = 2 * b / 5;
    xPos = three.CANVAS_WIDTH * 0.5;
    yPos = three.CANVAS_HEIGHT * 0.5 + 100 - 2.5 * dh;
    for(ind = maxEq; ind < maxEq+4; ind++ ) {
        yPos += dh; 
        eqInv[ind] = csComponents.createComponent(scene, 'invEqItem', '', 45, 45);
        eqInv[ind].snapCondition = snapOD; 
        gui.addChild(eqInv[ind]);
        eqInv[ind].setPosition(xPos, yPos, 1);
    }

    var b1 = csComponents.createButtonComponent(scene, 'Done', 290, 67);
    gui.addChild(b1);
    b1.setPosition(three.CANVAS_WIDTH * 0.5, gGY(20, maxcols), 1);        
    b1.onMouseUp = function () {
        gui.removeFromScene();
        //if (parentComp) {
        //   parentComp.addToScene();
        //}
        if (isFunction(endClbk)) {
            endClbk();
        }
    };

    return gui;
}


// ================================================
// map editor ui 
function createMapEditorUI(scene) {
    var maxcols = 20;
    var maxrows = 19;

    var idToChkBox = {};
    var chkbxItToObject = {};

    var createInstanceComponents = function( ) {
        // before callback
        var bClbk = function( v ) {
            return !v;
        };

        // update - (after change callback)
        var upRF = function( v, p, t, other ) {
            for( var it=0; it<il.length; it++ ) {
                if ( il[it] !== other) {
                    il[it].setState(false);
                }
                else {
                    // select in scene
                    selectObjectInScene(chkbxItToObject[it]);
                }
            }

            // update properties
        };

        // instance components
        var il = [];
        for(var ik in three.objects) {
            var chkbx = csComponents.createCheckBoxComponent(scene, three.objectProperties[three.objects[ik].id].name, 32, 32, bClbk, upRF, false);
            // order matters
            chkbxItToObject[il.length] = three.objects[ik];
            il[il.length] = chkbx; 
            idToChkBox[three.objects[ik].id] = chkbx;
        }

        return il;
    };

    // ================================================
    // instances
    var il = createInstanceComponents();

    var gui = csComponents.createComponent(scene, 'default', '', three.CANVAS_WIDTH*0.5, three.CANVAS_HEIGHT);
    csComponents.addChild(gui);
    gui.setPosition(three.CANVAS_WIDTH*0.75, three.CANVAS_HEIGHT*0.5, -3);

    // the list component
    var inst = csComponents.createListComponent(scene, 'label2', 290, 200);
    gui.addChild(inst);
    inst.setPosition(gGX(18.5, maxcols), gGY(3, maxrows), -3);        
    inst.setContents(il);

    // new button
    var newInst = csComponents.createNewButtonComponent(scene, '', 32, 32);
    gui.addChild(newInst);
    newInst.setPosition(gGX(15, maxcols), gGY(0.5, maxrows), -3);        
    newInst.onMouseUp = function () { 
        addInstance();
    };
    
    // duplicate button
    var dupInst = csComponents.createDuplicateButtonComponent(scene, '', 32, 32);
    gui.addChild(dupInst);
    dupInst.setPosition(gGX(15, maxcols), gGY(2.5, maxrows), -3);        
    dupInst.onMouseUp = function () {
        addInstance();
    };
    
    // delete button
    var delInst = csComponents.createDeleteButtonComponent(scene, '', 32, 32);
    gui.addChild(delInst);
    delInst.setPosition(gGX(15, maxcols), gGY(4.5, maxrows), -3);        
    delInst.onMouseUp = function () {
        delInstance();
    };

    //=====================================
    // properties 
    
    var properties = csComponents.createComponent(scene, 'label2', ' ', 290, 400);
    gui.addChild(properties);
    properties.setPosition(gGX(18.5, maxcols), gGY(14, maxrows), -4);        

    //=====================================
    // position x
    var posXL = csComponents.createComponent(scene, 'label', 'Position X', 150, 32);
    properties.addChild(posXL);
    posXL.setPosition(gGX(17.5, maxcols), gGY(8, maxrows), -3);        

    var posXP = csComponents.createHPButtonComponent(scene, '', 32, 32);
    properties.addChild(posXP);
    posXP.setPosition(gGX(20.5, maxcols), gGY(8, maxrows), -3);        
    posXP.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.x += 8;
        }
    };

    var posXM = csComponents.createHMButtonComponent(scene, '', 32, 32);
    properties.addChild(posXM);
    posXM.setPosition(gGX(20, maxcols), gGY(8, maxrows), -3);        
    posXM.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.x -= 8;
        }
    };

    //=====================================
    // position y
    var posYL = csComponents.createComponent(scene, 'label', 'Position Y', 150, 32);
    properties.addChild(posYL);
    posYL.setPosition(gGX(17.5, maxcols), gGY(9, maxrows), -3);        

    var posYP = csComponents.createHPButtonComponent(scene, '', 32, 32);
    properties.addChild(posYP);
    posYP.setPosition(gGX(20.5, maxcols), gGY(9, maxrows), -3);        
    posYP.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.y += 8;
        }
    };

    var posYM = csComponents.createHMButtonComponent(scene, '', 32, 32);
    properties.addChild(posYM);
    posYM.setPosition(gGX(20, maxcols), gGY(9, maxrows), -3);        
    posYM.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.y -= 8;
        }
    };

    //=====================================
    // position z
    var posZL = csComponents.createComponent(scene, 'label', 'Position Z', 150, 32);
    properties.addChild(posZL);
    posZL.setPosition(gGX(17.5, maxcols), gGY(10, maxrows), -3);        

    var posZP = csComponents.createHPButtonComponent(scene, '', 32, 32);
    properties.addChild(posZP);
    posZP.setPosition(gGX(20.5, maxcols), gGY(10, maxrows), -3);        
    posZP.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z += 8;
        }
    };

    var posZM = csComponents.createHMButtonComponent(scene, '', 32, 32);
    properties.addChild(posZM);
    posZM.setPosition(gGX(20, maxcols), gGY(10, maxrows), -3);        
    posZM.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    //=====================================
    // size x
    var sizeXL = csComponents.createComponent(scene, 'label', 'Size X', 150, 32);
    properties.addChild(sizeXL);
    sizeXL.setPosition(gGX(17.5, maxcols), gGY(11, maxrows), -3);        

    var sizeXP = csComponents.createHPButtonComponent(scene, '', 32, 32);
    properties.addChild(sizeXP);
    sizeXP.setPosition(gGX(20.5, maxcols), gGY(11, maxrows), -3);        
    sizeXP.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    var sizeXM = csComponents.createHMButtonComponent(scene, '', 32, 32);
    properties.addChild(sizeXM);
    sizeXM.setPosition(gGX(20, maxcols), gGY(11, maxrows), -3);        
    sizeXM.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    //=====================================
    // size y
    var sizeYL = csComponents.createComponent(scene, 'label', 'Size Y', 150, 32);
    properties.addChild(sizeYL);
    sizeYL.setPosition(gGX(17.5, maxcols), gGY(12, maxrows), -3);        

    var sizeYP = csComponents.createHPButtonComponent(scene, '', 32, 32);
    properties.addChild(sizeYP);
    sizeYP.setPosition(gGX(20.5, maxcols), gGY(12, maxrows), -3);        
    sizeYP.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    var sizeYM = csComponents.createHMButtonComponent(scene, '', 32, 32);
    properties.addChild(sizeYM);
    sizeYM.setPosition(gGX(20, maxcols), gGY(12, maxrows), -3);        
    sizeYM.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    //=====================================
    // size z
    var sizeZL = csComponents.createComponent(scene, 'label', 'Size Z', 150, 32);
    properties.addChild(sizeZL);
    sizeZL.setPosition(gGX(17.5, maxcols), gGY(13, maxrows), -3);        

    var sizeZP = csComponents.createHPButtonComponent(scene, '', 32, 32);
    properties.addChild(sizeZP);
    sizeZP.setPosition(gGX(20.5, maxcols), gGY(13, maxrows), -3);        
    sizeZP.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    var sizeZM = csComponents.createHMButtonComponent(scene, '', 32, 32);
    properties.addChild(sizeZM);
    sizeZM.setPosition(gGX(20, maxcols), gGY(13, maxrows), -3);        
    sizeZM.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };
    
    //=====================================
    // rot x
    var rotXL = csComponents.createComponent(scene, 'label', 'Rot X', 150, 32);
    properties.addChild(rotXL);
    rotXL.setPosition(gGX(17.5, maxcols), gGY(14, maxrows), -3);        

    var rotXP = csComponents.createHPButtonComponent(scene, '', 32, 32);
    properties.addChild(rotXP);
    rotXP.setPosition(gGX(20.5, maxcols), gGY(14, maxrows), -3);        
    rotXP.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    var rotXM = csComponents.createHMButtonComponent(scene, '', 32, 32);
    properties.addChild(rotXM);
    rotXM.setPosition(gGX(20, maxcols), gGY(14, maxrows), -3);        
    rotXM.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    //=====================================
    // rot y
    var rotYL = csComponents.createComponent(scene, 'label', 'Rot Y', 150, 32);
    properties.addChild(rotYL);
    rotYL.setPosition(gGX(17.5, maxcols), gGY(15, maxrows), -3);        

    var rotYP = csComponents.createHPButtonComponent(scene, '', 32, 32);
    properties.addChild(rotYP);
    rotYP.setPosition(gGX(20.5, maxcols), gGY(15, maxrows), -3);        
    rotYP.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    var rotYM = csComponents.createHMButtonComponent(scene, '', 32, 32);
    properties.addChild(rotYM);
    rotYM.setPosition(gGX(20, maxcols), gGY(15, maxrows), -3);        
    rotYM.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    //=====================================
    // rot z
    var rotZL = csComponents.createComponent(scene, 'label', 'Rot Z', 150, 32);
    properties.addChild(rotZL);
    rotZL.setPosition(gGX(17.5, maxcols), gGY(16, maxrows), -3);        

    var rotZP = csComponents.createHPButtonComponent(scene, '', 32, 32);
    properties.addChild(rotZP);
    rotZP.setPosition(gGX(20.5, maxcols), gGY(16, maxrows), -3);        
    rotZP.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    var rotZM = csComponents.createHMButtonComponent(scene, '', 32, 32);
    properties.addChild(rotZM);
    rotZM.setPosition(gGX(20, maxcols), gGY(16, maxrows), -3);        
    rotZM.onMouseUp = function () {
        if (three.objSelected) {
            three.objSelected.position.z -= 8;
        }
    };

    //=====================================
    // texture 
    var texL = csComponents.createComponent(scene, 'label', 'Tex', 50, 32);
    properties.addChild(texL);
    texL.setPosition(gGX(16.5, maxcols), gGY(17, maxrows), -3);     

    var texSL = csComponents.createValueListComponent(scene, '', ['Brl1', 'Bck1'], 0, 100, 32);
    properties.addChild(texSL);
    texSL.setPosition(gGX(19, maxcols), gGY(17, maxrows), -3);     

    //=====================================
    // load script
    var scriptBtn = csComponents.createSButtonComponent(scene, 'Script', 100, 32);
    properties.addChild(scriptBtn);
    scriptBtn.setPosition(gGX(17, maxcols), gGY(18, maxrows), -3);        
    scriptBtn.onMouseUp = function () {
        // load map
    };

    var scriptL = csComponents.createTextEditComponent(scene, 100, 32);
    properties.addChild(scriptL);
    scriptL.setText('none');
    scriptL.paint();
    scriptL.setPosition(gGX(19.5, maxcols), gGY(18, maxrows), -3);     

    //=====================================
    // character sheet
    var sheetBtn = csComponents.createSButtonComponent(scene, 'Sheet', 100, 32);
    properties.addChild(sheetBtn);
    sheetBtn.setPosition(gGX(17, maxcols), gGY(19, maxrows), -3);        
    sheetBtn.onMouseUp = function () {
        // show character sheet
        if (three.objSelected === three.player) {
            hideAllGameObjects();
            characterSheet = createCharacterCreationScreen(three.uiScene, null, three.characterSheet, showAllGameObjects); 
            characterSheet.addToScene();
        }        
        
    };

    var sheetL = csComponents.createTextEditComponent(scene, 150, 32);
    properties.addChild(sheetL);
    sheetL.setText('Fighter - CR 0');
    sheetL.paint();
    sheetL.setPosition(gGX(19.5, maxcols), gGY(19, maxrows), -3);     

    //=====================================
    // character sheet
    var invBtn = csComponents.createSButtonComponent(scene, 'Inv', 100, 32);
    properties.addChild(invBtn);
    invBtn.setPosition(gGX(17, maxcols), gGY(20, maxrows), -3);        
    invBtn.onMouseUp = function () {
        // show character inv 
        if (three.objSelected === three.player) {
            hideAllGameObjects();
            var uiInventoryScreen = createInventoryScreen(three.uiScene, gui, three.characterSheet, showAllGameObjects);
            uiInventoryScreen.addToScene();
        }        
    };

    var invL = csComponents.createTextEditComponent(scene, 150, 32);
    properties.addChild(invL);
    invL.setText('N/A');
    invL.paint();
    invL.setPosition(gGX(19.5, maxcols), gGY(20, maxrows), -3);  

    //=====================================
    // save/load 
   
    var b1 = csComponents.createSButtonComponent(scene, 'Load', 140, 40);
    gui.addChild(b1);
    b1.setPosition(gGX(17, maxcols), gGY(-1.5, maxcols), -3);        
    b1.onMouseUp = function () {
        // load map
        var file = 'src/ClockSkew.BetaMap.js';
        loadJS(file);
    };

    var b2 = csComponents.createSButtonComponent(scene, 'Save', 140, 40);
    gui.addChild(b2);
    b2.setPosition(gGX(20, maxcols), gGY(-1.5, maxcols), -3);        
    b2.onMouseUp = function () {
        // save map
        var prop = three.objectProperties;
        var mapList = [];

        for(var val in prop) {
            mapList[mapList.length] = prop[val];
        }

        var blob = new Blob(["var csMap = "+JSON.stringify(mapList)], {type: "text/plain;charset=utf-8"});
        saveAs(blob, "hello world.txt");
    };

    //=====================================
    // minimap

    var minimap = csComponents.createComponent(scene, 'label2', 'minimap', 200, 150);
    gui.addChild(minimap);
    minimap.setPosition(gGX(0.5, maxcols), gGY(1, maxrows), -3);  


    //=====================================
    // private functions
    
    // add instance
    var addInstance = function(x, y, z) {
        if (three.objSelected !== null ) {
            var prop = three.objectProperties[three.objSelected.id];
            var gObj = createGameObject( prop );

            if ( typeof ( x ) !== 'undefined' ) {
                gObj.position.set(x, y, z);
            }

            il = createInstanceComponents();
            inst.setContents( il );
        }
    };
    gui.addInstance = addInstance;

    // remove instance
    var delInstance = function() {
        if (three.objSelected !== null ) {
            removeGameObject(three.objSelected);

            il = createInstanceComponents();
            inst.setContents( il );
        }
    };
    gui.delInstance = delInstance;

    // update insances
    var updateInstances = function() {
        
    }; 

    //=====================================
    // public functions

    gui.selectObject = function(obj) {
        if (obj === null || typeof obj === 'undefined') {
            // TODO:
            // update properties
            return;
        }


        // select instance
        var chkbx = idToChkBox[obj.id];
        chkbx.toggleState(true);

        // move list
        for( var it in il ) {
            if ( il[it] === chkbx ) {
                inst.moveList(it);
            }
        }

        // TODO:
        // update properties
    };

    return gui;
}


// ================================================
// utilities

var gGX = function (col, numCols) {
    var xStep = three.CANVAS_WIDTH*0.8 / numCols;
    var xCoord = col*xStep + three.CANVAS_WIDTH*0.1;
    return xCoord;
};

var gGY = function (row, numRows) {
    var yStep = three.CANVAS_HEIGHT*0.8 / numRows;
    var yCoord = (numRows - row)*yStep + three.CANVAS_HEIGHT*0.1;

    return yCoord;
};
